package ml.talkntrade.android.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_reset_password.*
import ml.talkntrade.android.R
import ml.talkntrade.android.fragments.RequestPasswordResetFragment
import ml.talkntrade.android.utils.ProgressBarHandler

class ResetPasswordActivity : AppCompatActivity() {
    lateinit var progressBarHandler: ProgressBarHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.resetPasswordContainer, RequestPasswordResetFragment.newInstance())
                .commitNow()
        }

        goToLoginButton.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            finish()
        }

        progressBarHandler = ProgressBarHandler(this, resetPasswordLayout, resetPasswordProgressBar)
    }
}
