package ml.talkntrade.android.activities

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.apollographql.apollo.coroutines.toDeferred
import com.apollographql.apollo.exception.ApolloNetworkException
import kotlinx.android.synthetic.main.activity_report_user.*
import kotlinx.coroutines.*
import ml.talkntrade.android.activities.shared.BaseActivity
import ml.talkntrade.android.mutations.ReportUserMutation
import ml.talkntrade.android.type.ReportUserInput
import ml.talkntrade.android.utils.ProgressBarHandler
import ml.talkntrade.android.R
import ml.talkntrade.android.utils.GraphQLClient
import ml.talkntrade.android.utils.disable
import ml.talkntrade.android.utils.enable

class ReportUserActivity : BaseActivity() {
    private lateinit var progressBarHandler: ProgressBarHandler
    private var userId: String? = null
    private val categories = arrayOf(
        "", "copyright_infringement", "spamming", "malware_distribution", "other"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_user)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        snackBarView = reportUserCoordinatorLayout

        progressBarHandler = ProgressBarHandler(this, reportUserLinealLayout, reportUserProgressBar)

        val extras = intent.extras ?: return

        userId = extras.getString("USER_ID")

        val categoriesAdapter = ArrayAdapter.createFromResource(this, R.array.report_user_categories, android.R.layout.simple_spinner_item)
        categoriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        categorySpinner.adapter = categoriesAdapter
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.save, menu)
        return super.onCreateOptionsMenu(menu)
    }

    @ExperimentalCoroutinesApi
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.saveItem) {
            item.disable()
            progressBarHandler.show()

            val attributes = ReportUserInput(
                userId.toString(),
                categories[categorySpinner.selectedItemPosition],
                subjectEditText.text.toString(),
                messageEditText.text.toString()
            )
            val reportUserMutation = ReportUserMutation(attributes)
            val reportUserDeferred = GraphQLClient(this).apolloClient.mutate(reportUserMutation).toDeferred()

            CoroutineScope(Dispatchers.IO).launch {
                try {
                    val response = reportUserDeferred.await()
                    val reportUser = response.data()?.reportUser

                    withContext(Dispatchers.Main) {
                        Toast.makeText(this@ReportUserActivity, reportUser?.message, Toast.LENGTH_LONG).show()

                        if (reportUser?.success == true) {
                            finish()
                            onSupportNavigateUp()
                        }
                        else {
                            reportUser?.errors?.forEach {
                                if(it.path?.get(0) == "attributes") {
                                    when (it.path[1]) {
                                        "category" -> (categorySpinner.selectedView as TextView).error = it.message
                                        "subject" -> subjectEditText.error = it.message
                                        "message" -> messageEditText.error = it.message
                                    }
                                }
                            }
                            progressBarHandler.hide()
                            item.enable()
                        }
                    }
                }
                catch(e: ApolloNetworkException) {
                    handleApolloException(e)
                    withContext(Dispatchers.Main) {
                        progressBarHandler.hide()
                        item.enable()
                    }
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
