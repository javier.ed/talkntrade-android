package ml.talkntrade.android.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.LinkMovementMethod
import kotlinx.android.synthetic.main.activity_about.*
import ml.talkntrade.android.BuildConfig
import ml.talkntrade.android.R

class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        appNameTextView.text = getString(R.string.app_name_for_android, getString(R.string.app_name))
        versionTextView.text = getString(R.string.version, BuildConfig.VERSION_NAME)

        termsTextView.movementMethod = LinkMovementMethod.getInstance()
        privacyTextView.movementMethod = LinkMovementMethod.getInstance()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
