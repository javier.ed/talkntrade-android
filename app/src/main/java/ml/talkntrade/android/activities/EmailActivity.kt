package ml.talkntrade.android.activities

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.apollographql.apollo.coroutines.toChannel
import com.apollographql.apollo.coroutines.toDeferred
import com.apollographql.apollo.exception.ApolloNetworkException
import kotlinx.android.synthetic.main.activity_email.*
import kotlinx.android.synthetic.main.dialog_change_email.view.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.consumeEach
import ml.talkntrade.android.R
import ml.talkntrade.android.activities.shared.BaseActivity
import ml.talkntrade.android.mutations.ChangeEmailMutation
import ml.talkntrade.android.mutations.SendEmailVerificationTokenMutation
import ml.talkntrade.android.mutations.VerifyEmailMutation
import ml.talkntrade.android.queries.CurrentUserQuery
import ml.talkntrade.android.utils.GraphQLClient
import ml.talkntrade.android.utils.ProgressBarHandler

class EmailActivity : BaseActivity() {
    private lateinit var currentUserQuery: CurrentUserQuery
    private lateinit var currentUserJob: Job
    private lateinit var changeEmailDialog: AlertDialog
    private lateinit var progressBarHandler: ProgressBarHandler

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_email)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        snackBarView = emailCoordinatorLayout

        progressBarHandler = ProgressBarHandler(this, emailLinealLayout, emailProgressBar)

        loadEmail()
        loadVerifyEmailMutation()
        loadSendEmailVerificationTokenMutation()
        loadChangeEmailDialog()
    }

    override fun onDestroy() {
        super.onDestroy()
        currentUserJob.cancel()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.email, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.changeEmailItem) {
            changeEmailDialog.show()
        }
        return super.onOptionsItemSelected(item)
    }

    @ExperimentalCoroutinesApi
    private fun loadEmail() {
        currentUserQuery = CurrentUserQuery()
        val currentUserChannel = GraphQLClient(this).apolloClientWithCache.query(currentUserQuery).toChannel()

        currentUserJob = CoroutineScope(Dispatchers.IO).launch {
            try {
                currentUserChannel.consumeEach {
                    val currentUser = it.data()?.currentUser
                    withContext(Dispatchers.Main) {
                        emailTextView.text = currentUser?.email

                        if (currentUser?.emailVerified == false) {
                            showVerify()
                        }
                        else {
                            hideVerify()
                        }
                        progressBarHandler.hide()
                    }
                }
            }
            catch(e: ApolloNetworkException) {
                handleApolloException(e)
            }
        }
    }

    private fun showVerify() {
        unverifiedTextView.visibility = View.VISIBLE
        verifyEmailLayout.visibility = View.VISIBLE
        sendEmailVerificationCodeButtom.visibility = View.VISIBLE
    }

    private fun hideVerify() {
        unverifiedTextView.visibility = View.GONE
        verifyEmailLayout.visibility = View.GONE
        sendEmailVerificationCodeButtom.visibility = View.GONE
    }

    private fun loadVerifyEmailMutation() {
        verifyEmailButton.setOnClickListener {
            progressBarHandler.show()

            val token = verificationCodeEditText.text.toString()
            val verifyEmailMutation = VerifyEmailMutation(token)
            val verifyEmailDeferred = GraphQLClient(this).apolloClient.mutate(verifyEmailMutation).toDeferred()

            CoroutineScope(Dispatchers.IO).launch {
                try {
                    val response = verifyEmailDeferred.await()
                    val verifyEmail = response.data()?.verifyEmail

                    withContext(Dispatchers.Main) {
                        Toast.makeText(this@EmailActivity, verifyEmail?.message, Toast.LENGTH_LONG).show()
                        if (verifyEmail?.success == true) {
                            verificationCodeEditText.text.clear()
                            finish()
                        } else { progressBarHandler.hide() }
                    }
                } catch (e: ApolloNetworkException) {
                    handleApolloException(e)
                    withContext(Dispatchers.Main) {
                        progressBarHandler.hide()
                    }
                }
            }
        }
    }

    private fun loadSendEmailVerificationTokenMutation() {
        sendEmailVerificationCodeButtom.setOnClickListener {
            AlertDialog.Builder(this).setMessage(R.string.confirmation)
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    val sendEmailVerificationTokenMutation = SendEmailVerificationTokenMutation()
                    val sendEmailVerificationTokenDeferred = GraphQLClient(this).apolloClient
                        .mutate(sendEmailVerificationTokenMutation).toDeferred()

                    CoroutineScope(Dispatchers.IO).launch {
                        try {
                            val response = sendEmailVerificationTokenDeferred.await()
                            val sendEmailVerificationToken = response.data()?.sendEmailVerificationToken

                            withContext(Dispatchers.Main) {
                                Toast.makeText(this@EmailActivity, sendEmailVerificationToken?.message, Toast.LENGTH_LONG).show()
                            }
                        }
                        catch(e: ApolloNetworkException) {
                            handleApolloException(e)
                        }
                    }
                }
                .setNegativeButton(android.R.string.cancel) { dialog, _ ->
                    dialog.cancel()
                }
                .create().show()
        }
    }

    private fun loadChangeEmailDialog() {
        val changeEmailDialogView = layoutInflater.inflate(R.layout.dialog_change_email, null)
        changeEmailDialog = AlertDialog.Builder(this).setTitle(R.string.change_email).setView(changeEmailDialogView)
            .setCancelable(false)
            .setPositiveButton(android.R.string.ok, null)
            .setNegativeButton(android.R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .setOnDismissListener {
                changeEmailDialogView.passwordEditText.text.clear()
                changeEmailDialogView.passwordEditText.error = null
                changeEmailDialogView.emailEditText.text.clear()
                changeEmailDialogView.emailEditText.error = null
            }
            .create()

        changeEmailDialog.setOnShowListener {
            val okButton = changeEmailDialog.getButton(AlertDialog.BUTTON_POSITIVE)
            val cancelButton = changeEmailDialog.getButton(AlertDialog.BUTTON_NEGATIVE)

            okButton.setOnClickListener {
                okButton.isEnabled = false
                cancelButton.isEnabled = false
                val password = changeEmailDialogView.passwordEditText.text.toString()
                val email = changeEmailDialogView.emailEditText.text.toString()
                val changeEmailMutation = ChangeEmailMutation(password, email)
                val changeEmailDeferred = GraphQLClient(this).apolloClient.mutate(changeEmailMutation).toDeferred()

                CoroutineScope(Dispatchers.IO).launch {
                    try {
                        val response = changeEmailDeferred.await()
                        val changeEmail = response.data()?.changeEmail

                        withContext(Dispatchers.Main) {
                            if (changeEmail?.success == true) {
                                Toast.makeText(this@EmailActivity, changeEmail.message, Toast.LENGTH_LONG).show()
                                emailTextView.text = email
                                showVerify()
                                changeEmailDialog.dismiss()
                            } else {
                                Toast.makeText(this@EmailActivity, changeEmail?.message, Toast.LENGTH_LONG).show()

                                changeEmail?.errors?.forEach {
                                    when (it.path?.get(0)) {
                                        "password" -> changeEmailDialogView.passwordEditText.error = it.message
                                        "email" -> changeEmailDialogView.emailEditText.error = it.message
                                    }
                                }
                                okButton.isEnabled = true
                                cancelButton.isEnabled = true
                            }
                        }
                    }
                    catch (e: ApolloNetworkException) {
                        handleApolloException(e)
                        withContext(Dispatchers.Main) {
                            okButton.isEnabled = true
                            cancelButton.isEnabled = true
                        }
                    }
                }
            }
        }
    }
}
