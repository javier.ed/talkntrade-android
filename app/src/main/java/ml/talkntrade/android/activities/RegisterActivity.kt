package ml.talkntrade.android.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import com.apollographql.apollo.coroutines.toDeferred
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register.passwordEditText
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ml.talkntrade.android.R
import ml.talkntrade.android.TalkntradeApplication
import ml.talkntrade.android.data.CurrentSession
import ml.talkntrade.android.data.CurrentUser
import ml.talkntrade.android.mutations.RegisterMutation
import ml.talkntrade.android.type.UserInput
import ml.talkntrade.android.utils.GraphQLClient
import ml.talkntrade.android.utils.ProgressBarHandler
import java.lang.Exception

class RegisterActivity : AppCompatActivity() {
    lateinit var progressBarHandler: ProgressBarHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        progressBarHandler = ProgressBarHandler(this, registerLayout, registerProgressBar)

        // Set up the register form.
        passwordEditText.setOnEditorActionListener(TextView.OnEditorActionListener { _, id, _ ->
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptRegister()
                return@OnEditorActionListener true
            }
            false
        })

        registerButton.setOnClickListener { attemptRegister() }

        goToLoginButton.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            finish()
        }
    }

    private fun attemptRegister() {
        progressBarHandler.show()

        val attributes = UserInput(
            usernameEditText.text.toString(),
            emailEditText.text.toString(),
            passwordEditText.text.toString(),
            passwordConfirmationEditText.text.toString()
        )

        val registerMutation = RegisterMutation(attributes)
        val registerDeferred = GraphQLClient(this).apolloClient.mutate(registerMutation).toDeferred()

        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = registerDeferred.await().data()
                val register = response?.register

                withContext(Dispatchers.Main) {
                    if (register?.success == true) {
                        Toast.makeText(this@RegisterActivity, register.message, Toast.LENGTH_LONG).show()
                        CurrentUser(this@RegisterActivity).update(register.user?.id, register.user?.username)
                        CurrentSession(this@RegisterActivity).update(register.sessionId)
                        val intent = Intent(this@RegisterActivity, MainActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        startActivity(intent)
                        finish()
                    }
                    else {
                        Toast.makeText(this@RegisterActivity, register?.message, Toast.LENGTH_LONG).show()
                        register?.errors?.map {
                            when (it.path?.get(1)) {
                                "username" -> usernameEditText.error = it.message
                                "email" -> emailEditText.error = it.message
                                "password" -> passwordEditText.error = it.message
                                "passwordConfirmation" -> passwordConfirmationEditText.error = it.message
                            }
                        }
                        progressBarHandler.hide()
                    }
                }
            }
            catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    GraphQLClient.handleError(this@RegisterActivity, e)
                    progressBarHandler.hide()
                }
            }
        }
    }
}
