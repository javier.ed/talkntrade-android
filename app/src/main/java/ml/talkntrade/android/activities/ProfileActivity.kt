package ml.talkntrade.android.activities

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import com.apollographql.apollo.api.Input
import com.apollographql.apollo.coroutines.toChannel
import com.apollographql.apollo.coroutines.toDeferred
import com.apollographql.apollo.exception.ApolloNetworkException
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.consumeEach
import ml.talkntrade.android.R
import ml.talkntrade.android.activities.shared.BaseActivity
import ml.talkntrade.android.mutations.UpdateProfileMutation
import ml.talkntrade.android.queries.CurrentUserQuery
import ml.talkntrade.android.type.UserProfileInput
import ml.talkntrade.android.utils.*
import java.util.*


class ProfileActivity : BaseActivity(), DatePickerDialog.OnDateSetListener {
    private lateinit var progressBarHandler: ProgressBarHandler
    private val countries = Locale.getISOCountries().map {
        arrayOf(it, Locale("", it).displayCountry)
    }

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        snackBarView = profileCoordinatorLayout

        progressBarHandler = ProgressBarHandler(this, profileLinealLayout, profileProgressBar)

        val calendar = Calendar.getInstance()

        val datePickerDialog = DatePickerDialog(
            this,
            this@ProfileActivity,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )

        birthdateEditText.setOnFocusChangeListener { _, hasFocus -> if (hasFocus) datePickerDialog.show() }
        birthdateEditText.setOnClickListener { datePickerDialog.show() }
        birthdateEditText.doOnTextChanged { text, _, _, _ ->
            if (text?.isNotEmpty() == true) { clearBirthdateButton.visibility = View.VISIBLE }
            else { clearBirthdateButton.visibility = View.GONE }
        }
        clearBirthdateButton.setOnClickListener { birthdateEditText.text.clear() }

        val countriesAdapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_dropdown_item_1line,
            countries.map { it[1] }
        )
        countryAutoComplete.setAdapter(countriesAdapter)
        countryAutoComplete.setOnFocusChangeListener { _, hasFocus ->
            val text = countryAutoComplete.text.toString()
            if (!hasFocus && text.isNotEmpty()) {
                val country = countries.find { it[1].toRegex(RegexOption.IGNORE_CASE).matchEntire(text) != null }
                if (country == null) { countryAutoComplete.text.clear() }
                else if (countries.find { it[1] == text } == null) { countryAutoComplete.setText(country[1]) }
            }
        }

        val currentUserQuery = CurrentUserQuery()
        val currentUserChannel = GraphQLClient(this).apolloClientWithCache.query(currentUserQuery).toChannel()

        CoroutineScope(Dispatchers.IO).launch {
            try {
                currentUserChannel.consumeEach {
                    val profile = it.data()?.currentUser?.profile

                    withContext(Dispatchers.Main) {
                        progressBarHandler.hide()
                        displayNameEditText.setText(profile?.displayName)
                        firstNameEditText.setText(profile?.firstName)
                        lastNameEditText.setText(profile?.lastName)
                        if (profile?.birthdate != null) {
                            birthdateEditText.setText(DateHelper.stringify(profile.birthdate, DateHelper.DATE_FORMAT))
                            calendar.timeZone = TimeZone.getTimeZone("GMT")
                            calendar.time = profile.birthdate
                            datePickerDialog.updateDate(
                                calendar.get(Calendar.YEAR),
                                calendar.get(Calendar.MONTH),
                                calendar.get(Calendar.DAY_OF_MONTH)
                            )
                        }
                        countryAutoComplete.setText(countries.find { c -> c[0] == profile?.countryGec }?.get(1))
                        bioEditText.setText(profile?.bio)
                    }
                }
            }
            catch (e: ApolloNetworkException) {
                handleApolloException(e)
                withContext(Dispatchers.Main) {
                    progressBarHandler.hide()
                }
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.save, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        birthdateEditText.setText(getString(R.string.date_selected, year, month + 1, dayOfMonth))
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.saveItem) {
            item.disable()
            progressBarHandler.show()

            var country = countries.find { c -> c[1] == countryAutoComplete.text.toString() }?.get(0)
            if (country == null) { country = "" }

            val attributes = UserProfileInput(
                displayNameEditText.text.toString(),
                firstNameEditText.text.toString(),
                lastNameEditText.text.toString(),
                Input.optional(DateHelper(). parse(birthdateEditText.text.toString(), DateHelper.DATE_FORMAT)),
                country,
                bioEditText.text.toString()
            )
            val updateProfileMutation = UpdateProfileMutation(attributes)
            val updateProfileDeferred = GraphQLClient(this).apolloClient.mutate(updateProfileMutation).toDeferred()

            CoroutineScope(Dispatchers.IO).launch {
                try {
                    val response = updateProfileDeferred.await()
                    val updateProfile = response.data()?.updateProfile

                    withContext(Dispatchers.Main) {
                        if (updateProfile?.success == true) {
                            Toast.makeText(this@ProfileActivity, updateProfile.message, Toast.LENGTH_LONG).show()
                            finish()
                            onSupportNavigateUp()
                        }
                        else {
                            Toast.makeText(this@ProfileActivity, updateProfile?.message, Toast.LENGTH_LONG).show()
                            updateProfile?.errors?.forEach {
                                if(it.path?.get(0) == "attributes") {
                                    when (it.path[1]) {
                                        "displayName" -> displayNameEditText.error = it.message
                                        "firstName" -> firstNameEditText.error = it.message
                                        "lastName" -> lastNameEditText.error = it.message
                                        "birthdate" -> birthdateEditText.error = it.message
                                        "country" -> countryAutoComplete.error = it.message
                                        "bio" -> bioEditText.error = it.message
                                    }
                                }
                            }
                            progressBarHandler.hide()
                            item.enable()
                        }
                    }

                }
                catch (e: ApolloNetworkException) {
                    handleApolloException(e)
                    withContext(Dispatchers.Main) {
                        progressBarHandler.hide()
                        item.enable()
                    }
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
