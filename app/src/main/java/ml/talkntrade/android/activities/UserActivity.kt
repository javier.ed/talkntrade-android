package ml.talkntrade.android.activities

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.apollographql.apollo.coroutines.toChannel
import com.apollographql.apollo.coroutines.toDeferred
import com.apollographql.apollo.exception.ApolloNetworkException
import kotlinx.android.synthetic.main.activity_user.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.consumeEach
import ml.talkntrade.android.R
import ml.talkntrade.android.activities.shared.BaseActivity
import ml.talkntrade.android.data.CurrentUser
import ml.talkntrade.android.mutations.BlockUserMutation
import ml.talkntrade.android.queries.UserQuery
import ml.talkntrade.android.utils.AvatarLoader
import ml.talkntrade.android.utils.GraphQLClient
import ml.talkntrade.android.utils.ProgressBarHandler
import java.util.*

class UserActivity : BaseActivity() {
    private var userId = ""
    private lateinit var userJob: Job
    private lateinit var progressBarHandler: ProgressBarHandler
    private var blockUserItem: MenuItem? = null
    private var reportUserItem: MenuItem? = null

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        snackBarView = userCoordinatorLayout

        progressBarHandler = ProgressBarHandler(this, userLinealLayout, userProgressBar)

        val extras = intent.extras ?: return
        userId = extras.getString("ID") as String

        loadUser()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    @ExperimentalCoroutinesApi
    override fun onRestart() {
        super.onRestart()
        loadUser()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::userJob.isInitialized) { userJob.cancel() }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.user, menu)
        blockUserItem = menu?.findItem(R.id.blockUserItem)
        reportUserItem = menu?.findItem(R.id.reportUserItem)
        return super.onCreateOptionsMenu(menu)
    }

    @ExperimentalCoroutinesApi
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            R.id.blockUserItem -> {
                val blockUserMutation = BlockUserMutation(userId)
                val blockUserDeferred = GraphQLClient(this).apolloClient.mutate(blockUserMutation).toDeferred()

                CoroutineScope(Dispatchers.IO).launch {
                    try {
                        val response = blockUserDeferred.await().data()
                        val blockUser = response?.blockUser

                        withContext(Dispatchers.Main) {
                            if (blockUser?.success == true) {
                                Toast.makeText(this@UserActivity, blockUser.message, Toast.LENGTH_LONG).show()
                                val intent = Intent(this@UserActivity, MainActivity::class.java)
                                startActivity(intent)
                                finish()
                            }
                            else {
                                Toast.makeText(this@UserActivity, blockUser?.message, Toast.LENGTH_LONG).show()
                            }
                        }
                    }
                    catch(e: ApolloNetworkException) {
                        handleApolloException(e)
                    }
                }
            }
            R.id.reportUserItem -> {
                val intent = Intent(this, ReportUserActivity::class.java)
                intent.putExtra("USER_ID", userId)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    @ExperimentalCoroutinesApi
    private fun loadUser() {
        val userQuery = UserQuery(userId)
        val userChannel = GraphQLClient(this).apolloClientWithCache.query(userQuery).toChannel()

        userJob = CoroutineScope(Dispatchers.IO).launch {
            try {
                userChannel.consumeEach {
                    val user = it.data()?.user

                    withContext(Dispatchers.Main) {
                        if (user != null) {
                            if (user.id != CurrentUser(this@UserActivity).id) {
                                blockUserItem?.isVisible = true
                                reportUserItem?.isVisible = true
                            }

                            AvatarLoader(user.username.toString(), avatarImageView)
                            displayNameTextView.text = user.profile?.displayName
                            usernameTextView.text = getString(R.string.at_username, user.username)

                            if (user.profile?.countryGec?.isNotEmpty() == true) {
                                countryTextView.text = Locale("", user.profile.countryGec).displayCountry
                                countryTextView.visibility = View.VISIBLE
                            }

                            if (user.profile?.bio?.isNotEmpty() == true) {
                                bioTextView.text = user.profile.bio
                                bioTextView.visibility = View.VISIBLE
                            }

                            progressBarHandler.hide()
                        }
                        else {
                            Toast.makeText(this@UserActivity, R.string.user_not_found, Toast.LENGTH_LONG).show()
                            refreshConnectionStatus()
                            this@UserActivity.onSupportNavigateUp()
                        }
                    }
                }
            }
            catch (e: ApolloNetworkException) {
                handleApolloException(e)
                withContext(Dispatchers.Main) {
                    this@UserActivity.onSupportNavigateUp()
                }
            }
        }
    }
}
