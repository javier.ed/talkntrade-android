package ml.talkntrade.android.activities

import android.content.Intent
import android.os.Bundle
import com.apollographql.apollo.coroutines.toChannel
import com.apollographql.apollo.exception.ApolloNetworkException
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.consumeEach
import ml.talkntrade.android.BuildConfig
import ml.talkntrade.android.R
import ml.talkntrade.android.activities.shared.BaseActivity
import ml.talkntrade.android.queries.CurrentUserQuery
import ml.talkntrade.android.utils.GraphQLClient
import ml.talkntrade.android.utils.ProgressBarHandler

class SettingsActivity : BaseActivity() {
    private lateinit var currentUserJob: Job
    private lateinit var progressBarHandler: ProgressBarHandler

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        snackBarView = settingsCoordinatorLayout

        progressBarHandler = ProgressBarHandler(this, settingsLinealLayout, settingsProgressBar)

        appNameWithVersionTextView.text = getString(
            R.string.app_name_for_android_with_version,
            getString(R.string.app_name),
            BuildConfig.VERSION_NAME
        )

        updateProfileItem.setOnClickListener {
            val intent = Intent(this, ProfileActivity::class.java)
            startActivity(intent)
        }

        changePasswordItem.setOnClickListener {
            val intent = Intent(this, PasswordActivity::class.java)
            startActivity(intent)
        }

        emailAddressItem.setOnClickListener {
            val intent = Intent(this, EmailActivity::class.java)
            startActivity(intent)
        }

        blockedUsersItem.setOnClickListener {
            val intent = Intent(this, BlockedUsersActivity::class.java)
            startActivity(intent)
        }

        aboutItem.setOnClickListener {
            val intent = Intent(this, AboutActivity::class.java)
            startActivity(intent)
        }

        loadCurrentUser()
    }

    @ExperimentalCoroutinesApi
    override fun onRestart() {
        loadCurrentUser()
        super.onRestart()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::currentUserJob.isInitialized) { currentUserJob.cancel() }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    @ExperimentalCoroutinesApi
    private fun loadCurrentUser() {
        val currentUserQuery = CurrentUserQuery()
        val currentUserChannel = GraphQLClient(this).apolloClientWithCache.query(currentUserQuery).toChannel()

        currentUserJob = CoroutineScope(Dispatchers.IO).launch {
            try {
                currentUserChannel.consumeEach {
                    val currentUser = it.data()?.currentUser
                    withContext(Dispatchers.Main) {
                        profileTextView.text = currentUser?.profile?.displayName

                        emailTextView.text = if (currentUser?.emailVerified == true) {
                            currentUser.email
                        }
                        else {
                            getString(R.string.email_unverified, currentUser?.email)
                        }

                        blockedUsersCount.text = getString(R.string.blocked_users_count, currentUser?.blockedUsers?.size)

                        progressBarHandler.hide()
                    }
                }
            }
            catch(e: ApolloNetworkException) {
                handleApolloException(e)
                withContext(Dispatchers.Main) {
                    progressBarHandler.hide()
                }
            }
        }
    }
}
