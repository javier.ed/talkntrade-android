package ml.talkntrade.android.activities

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.apollographql.apollo.coroutines.toChannel
import com.apollographql.apollo.exception.ApolloNetworkException
import kotlinx.android.synthetic.main.activity_blocked_users.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.consumeEach
import ml.talkntrade.android.R
import ml.talkntrade.android.activities.shared.BaseActivity
import ml.talkntrade.android.adapters.BlockedUsersAdapter
import ml.talkntrade.android.queries.CurrentUserBlockedUsersQuery
import ml.talkntrade.android.utils.GraphQLClient
import ml.talkntrade.android.utils.ProgressBarHandler

class BlockedUsersActivity : BaseActivity() {
    private lateinit var blockedUsersJob: Job
    private lateinit var progressBarHandler: ProgressBarHandler

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_blocked_users)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        snackBarView = blockedUsersCoordinatorLayout

        progressBarHandler = ProgressBarHandler(this, blockedUsersRecyclerView, blockedUsersProgressBar)

        loadBlockedUsers()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    @ExperimentalCoroutinesApi
    override fun onRestart() {
        super.onRestart()
        loadBlockedUsers()
    }

    override fun onDestroy() {
        super.onDestroy()
        if(::blockedUsersJob.isInitialized) { blockedUsersJob.cancel() }
    }

    @ExperimentalCoroutinesApi
    fun loadBlockedUsers() {
        blockedUsersRecyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
        }

        val blockedUsersQuery = CurrentUserBlockedUsersQuery()
        val blockedUsersChannel = GraphQLClient(this).apolloClient.query(blockedUsersQuery).toChannel()

        blockedUsersJob = CoroutineScope(Dispatchers.IO).launch {
            try {
                blockedUsersChannel.consumeEach {
                    val blockedUsers = it.data()?.currentUser?.blockedUsers

                    withContext(Dispatchers.Main) {
                        blockedUsersRecyclerView.adapter = BlockedUsersAdapter(
                            blockedUsers?.toMutableList() as MutableList<CurrentUserBlockedUsersQuery.BlockedUser>,
                            this@BlockedUsersActivity
                        )
                        progressBarHandler.hide()
                    }
                }
            }
            catch(e: ApolloNetworkException) {
                handleApolloException(e)
                withContext(Dispatchers.Main) {
                    progressBarHandler.hide()
                }
            }
        }
    }
}
