package ml.talkntrade.android.activities.shared

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.apollographql.apollo.exception.ApolloNetworkException
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.withContext
import ml.talkntrade.android.R
import ml.talkntrade.android.activities.LoginActivity
import ml.talkntrade.android.data.CurrentDevice
import ml.talkntrade.android.data.CurrentSession
import ml.talkntrade.android.data.CurrentUser
import ml.talkntrade.android.models.NewMessage
import ml.talkntrade.android.services.ConnectionService
import ml.talkntrade.android.utils.GraphQLClient

abstract class BaseActivity : AppCompatActivity() {
    protected lateinit var snackBarView : View
    private lateinit var connectionServiceIntent : Intent
    private lateinit var connectionService: ConnectionService
    private lateinit var listener : BaseActivityListener

    private val connectionServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val binder = service as ConnectionService.ConnectionServiceBinder
            connectionService = binder.getService()
            if (connectionService.currentStatusCode != ConnectionService.STATUS_CONNECTED) {
                showStatusCodeMessage(connectionService.currentStatusCode)
            }
            connectionService.addListener("BaseActivity", object : ConnectionService.ConnectionServiceListener {
                override fun onStatusChange(statusCode: Int): Boolean {
                    showStatusCodeMessage(statusCode)
                    return true
                }

                override fun onNewMessage(message: NewMessage) = if (::listener.isInitialized) { listener.onNewMessage(message) } else { true }
            })
        }

        override fun onServiceDisconnected(name: ComponentName?) { }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setOnNewMessageListener { true }
        connectionServiceIntent = Intent(this, ConnectionService::class.java)
        startService(connectionServiceIntent).also {
            bindService(connectionServiceIntent, connectionServiceConnection, Context.BIND_AUTO_CREATE)
        }
    }

    override fun onRestart() {
        super.onRestart()
        bindService(connectionServiceIntent, connectionServiceConnection, Context.BIND_AUTO_CREATE)
    }

    override fun onStop() {
        super.onStop()
        unbindService(connectionServiceConnection)
    }

    private fun showStatusCodeMessage(statusCode: Int) {
        when (statusCode) {
            ConnectionService.STATUS_DISCONNECTED -> showSnackBar(getString(R.string.no_connection), Snackbar.LENGTH_INDEFINITE)
            ConnectionService.STATUS_CONNECTING -> showSnackBar(getString(R.string.waiting_for_connection), Snackbar.LENGTH_INDEFINITE)
            ConnectionService.STATUS_CONNECTED -> showSnackBar(getString(R.string.connected))
            ConnectionService.STATUS_DEVICE_AUTHORIZED -> clearAndRedirectToLogin()
            ConnectionService.STATUS_DEVICE_UNAUTHORIZED -> {
                CurrentDevice(this@BaseActivity).delete()
                clearAndRedirectToLogin()
            }
            ConnectionService.STATUS_CONNECTION_ERROR -> showSnackBar(getString(R.string.connection_error), Snackbar.LENGTH_INDEFINITE)
            else -> showSnackBar(getString(R.string.unknown_error), Snackbar.LENGTH_INDEFINITE)
        }
    }

    private fun showSnackBar(message: String, length: Int = Snackbar.LENGTH_SHORT) {
        if (::snackBarView.isInitialized) {
            val snack = Snackbar.make(snackBarView, message, length)
            snack.setAction(R.string.hide) {}
            snack.show()
        }
        else { Toast.makeText(this, message, Toast.LENGTH_SHORT).show() }
    }

    protected fun clearAndRedirectToLogin() {
        CurrentSession(this).delete()
        CurrentUser(this).delete()
        GraphQLClient(this).apolloClientWithCache.clearNormalizedCache()
        stopService(connectionServiceIntent)
        val intent = Intent(this, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
        finish()
    }

    @ExperimentalCoroutinesApi
    fun refreshConnectionStatus() {
        connectionService.refreshConnectionStatus()
    }

    @ExperimentalCoroutinesApi
    suspend fun handleApolloException(e: ApolloNetworkException) {
        withContext(Dispatchers.Main) {
            Toast.makeText(this@BaseActivity, R.string.failed_to_execute_request, Toast.LENGTH_LONG).show()
        }
        e.printStackTrace()
        refreshConnectionStatus()
    }

    fun setOnNewMessageListener(event: (NewMessage) -> Boolean) {
        listener = object:  BaseActivityListener {
            override fun onNewMessage(message: NewMessage) = event(message)
        }
    }

    interface BaseActivityListener {
        fun onNewMessage(message: NewMessage) : Boolean
    }
}
