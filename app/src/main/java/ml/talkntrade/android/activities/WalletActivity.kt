package ml.talkntrade.android.activities

import android.annotation.SuppressLint
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.zxing.BarcodeFormat
import com.google.zxing.qrcode.QRCodeWriter
import kotlinx.android.synthetic.main.activity_wallet.*
import kotlinx.android.synthetic.main.dialog_deposit.view.*
import ml.talkntrade.android.R
import ml.talkntrade.android.queries.AssetsQuery
import ml.talkntrade.android.utils.GraphQLClient
import android.graphics.Bitmap
import android.graphics.Color
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.LinearLayoutManager
import com.apollographql.apollo.coroutines.toChannel
import com.apollographql.apollo.coroutines.toDeferred
import com.apollographql.apollo.exception.ApolloNetworkException
import kotlinx.android.synthetic.main.dialog_withdraw.view.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.consumeEach
import ml.talkntrade.android.activities.shared.BaseActivity
import ml.talkntrade.android.adapters.TransactionsAdapter
import ml.talkntrade.android.mutations.WithdrawMutation
import ml.talkntrade.android.type.WithdrawInput
import ml.talkntrade.android.utils.ProgressBarHandler

class WalletActivity : BaseActivity() {
    private val currentCurrencyIsoCode = "BTC"
    private lateinit var depositDialogView: View
    private lateinit var depositDialog: AlertDialog
    private lateinit var withdrawDialogView: View
    private lateinit var withdrawDialog: AlertDialog
    private lateinit var assetJob: Job
    private var assetLoaded = false
    private lateinit var progressBarHandler: ProgressBarHandler

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallet)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        snackBarView = walletCoordinatorLayout

        progressBarHandler = ProgressBarHandler(this, walletLinealLayout, walletProgressBar)

        loadAsset()
    }

    override fun onDestroy() {
        super.onDestroy()
        assetJob.cancel()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    @ExperimentalCoroutinesApi
    private fun loadAsset() {
        val assetsQuery = AssetsQuery()
        val assetsChannel = GraphQLClient(this).apolloClientWithCache.query(assetsQuery).toChannel()

        assetJob = CoroutineScope(Dispatchers.IO).launch {
            try {
                assetsChannel.consumeEach {
                    val asset = it.data()?.assets?.first()

                    if (asset != null) {
                        withContext(Dispatchers.Main) {
                            currency.text = asset.currency?.isoCode

                            balance.text = getString(R.string.amount_format, asset.currency?.exponent).format(asset.balance)

                            loadDepositDialog(asset)
                            loadWithdrawDialog(asset)

                            transactionsRecyclerView.apply {
                                setHasFixedSize(true)
                                layoutManager = LinearLayoutManager(this@WalletActivity)
                            }

                            asset.transactions?.let {
                                transactionsRecyclerView.adapter = TransactionsAdapter(asset.transactions, asset.currency)
                            }

                            progressBarHandler.hide()
                        }
                        assetLoaded = true
                    }
                }
            }
            catch (e: ApolloNetworkException) {
                handleApolloException(e)
            }
        }
    }

    @SuppressLint("InflateParams")
    private fun loadDepositDialog(asset: AssetsQuery.Asset?) {
        if (!assetLoaded) {
            depositDialogView = layoutInflater.inflate(R.layout.dialog_deposit, null)
            depositDialog =
                AlertDialog.Builder(this).setTitle(R.string.deposit).setView(depositDialogView)
                    .setPositiveButton(R.string.close, null).create()

            depositDialogView.copyAddressButton.setOnClickListener {
                val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val clip = ClipData.newPlainText(getString(R.string.my_address), depositDialogView.addressTextView.text)
                clipboard.primaryClip = clip
            }

            depositButton.setOnClickListener { depositDialog.show() }
        }

        val params = asset?.currency?.allowedTransactions?.find { t -> t.id == "deposit" }

        if (params != null) {
            depositDialogView.minAmountTextView.text =
                getString(R.string.amount_iso_code_format, asset.currency.exponent, asset.currency.isoCode)
                    .format(params.minAmount?.plus(params.minFee as Double))
            depositDialogView.minFeeTextView.text =
                getString(R.string.amount_iso_code_format, asset.currency.exponent, asset.currency.isoCode)
                    .format(params.minFee)

            val highAmount = params.minFee?.div(params.feePercentage as Double)

            depositDialogView.feePercentageTextView.text =
                getString(R.string.fee_percentage, highAmount, asset.currency.isoCode, params.feePercentage?.times(100).toString())
        }

        depositDialogView.addressTextView.text = asset?.primaryAddress.toString()

        val qrCodeWriter = QRCodeWriter()
        val bitMatrix = qrCodeWriter.encode(asset?.primaryAddress.toString(), BarcodeFormat.QR_CODE, 360, 360)
        val width = bitMatrix.width - 90
        val height = bitMatrix.height - 90
        val bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
        for (x in 0 until width) {
            for (y in 0 until height) {
                bmp.setPixel(x, y, if (bitMatrix.get(x + 45, y + 45)) Color.BLACK else Color.WHITE)
            }
        }

        depositDialogView.qrCodeImageView.setImageBitmap(bmp)
    }

    @SuppressLint("InflateParams")
    @ExperimentalCoroutinesApi
    private fun loadWithdrawDialog(asset: AssetsQuery.Asset?) {
        if (!assetLoaded) {
            withdrawDialogView = layoutInflater.inflate(R.layout.dialog_withdraw, null)
            withdrawDialog =
                AlertDialog.Builder(this).setTitle(R.string.withdraw).setView(withdrawDialogView)
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.ok, null)
                    .setNegativeButton(android.R.string.cancel) { dialog, _ ->
                        dialog.dismiss()
                    }
                    .setOnDismissListener {
                        withdrawDialogView.addressEditText.text.clear()
                        withdrawDialogView.addressEditText.error = null
                        withdrawDialogView.amountEditText.setText(R.string.amount_hint)
                        withdrawDialogView.amountEditText.error = null
                    }
                    .create()

            withdrawDialog.setOnShowListener {
                val okButton = withdrawDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                val cancelButton = withdrawDialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                okButton.setOnClickListener {
                    okButton.isEnabled = false
                    cancelButton.isEnabled = false

                    val amount = try {
                        withdrawDialogView.amountEditText.text.toString().toDouble()
                    } catch (e: NumberFormatException) {
                        0.0
                    }

                    val withdrawInput = WithdrawInput(
                        currentCurrencyIsoCode,
                        amount,
                        withdrawDialogView.addressEditText.text.toString()
                    )
                    val withdrawMutation = WithdrawMutation(withdrawInput)
                    val withdrawDeferred =
                        GraphQLClient(this).apolloClient.mutate(withdrawMutation).toDeferred()

                    CoroutineScope(Dispatchers.IO).launch {
                        try {
                            val response = withdrawDeferred.await().data()
                            val withdraw = response?.withdraw

                            withContext(Dispatchers.Main) {
                                if (withdraw?.success == true) {
                                    Toast.makeText(
                                        this@WalletActivity,
                                        withdraw.message,
                                        Toast.LENGTH_LONG
                                    ).show()
                                    loadAsset()
                                    withdrawDialog.dismiss()
                                } else {
                                    withdraw?.errors?.map {
                                        when (it.path?.get(1)) {
                                            "receiverAddress" -> withdrawDialogView.addressEditText.error =
                                                it.message
                                            "amount" -> withdrawDialogView.amountEditText.error =
                                                it.message
                                        }
                                    }
                                    Toast.makeText(
                                        this@WalletActivity,
                                        withdraw?.message,
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                                okButton.isEnabled = true
                                cancelButton.isEnabled = true
                            }
                        } catch (e: ApolloNetworkException) {
                            handleApolloException(e)
                            withContext(Dispatchers.Main) {
                                okButton.isEnabled = true
                                cancelButton.isEnabled = true
                            }
                        }
                    }
                }
            }

            withdrawalButton.setOnClickListener { withdrawDialog.show() }
        }

        val currencyString = getString(R.string.amount_iso_code_format, asset?.currency?.exponent, asset?.currency?.isoCode)

        withdrawDialogView.availableBalanceTextView.text = currencyString.format(asset?.balance)

        val params = asset?.currency?.allowedTransactions?.find { t -> t.id == "withdrawal" }

        if (params != null) {
            withdrawDialogView.amountEditText.addTextChangedListener { a ->
                val amount = try {
                    a.toString().toDouble()
                } catch (e: NumberFormatException) {
                    0.0
                }

                when {
                    amount < params.minAmount as Double -> {
                        withdrawDialogView.totalAmountTextView.text = currencyString.format(0.0)
                        withdrawDialogView.feeTextView.text = currencyString.format(0.0)
                        withdrawDialogView.willReceiveTextView.text = currencyString.format(0.0)
                    }
                    ((amount * params.feePercentage as Double) / (1 - params.feePercentage)) <= params.minFee as Double -> {
                        withdrawDialogView.totalAmountTextView.text =
                            currencyString.format(amount + params.minFee)
                        withdrawDialogView.feeTextView.text = currencyString.format(-params.minFee)
                        withdrawDialogView.willReceiveTextView.text = currencyString.format(amount)
                    }
                    else -> {
                        val fee = ((amount * params.feePercentage) / (1 - params.feePercentage))
                        withdrawDialogView.totalAmountTextView.text =
                            currencyString.format(amount + fee)
                        withdrawDialogView.feeTextView.text = currencyString.format(fee)
                        withdrawDialogView.willReceiveTextView.text = currencyString.format(amount)
                    }
                }
            }
        }
    }
}
