package ml.talkntrade.android.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.viewpager2.widget.ViewPager2
import com.apollographql.apollo.exception.ApolloNetworkException
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_talk.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.consumeEach
import ml.talkntrade.android.R
import ml.talkntrade.android.activities.shared.BaseActivity
import ml.talkntrade.android.adapters.TalkPagerAdapter
import ml.talkntrade.android.collections.TalkFragments
import ml.talkntrade.android.data.CurrentUser
import ml.talkntrade.android.utils.AvatarLoader
import ml.talkntrade.android.utils.Mutations
import ml.talkntrade.android.utils.Queries

/**
 * Talk activity
 *
 * Intent extras:
 * - ID: ID of the talk
 * - USER_ID: ID of the other user if the talk doesn't exists
 */
class TalkActivity : BaseActivity() {
    private var userId: String? = null
    private var talkId: String? = null
    private lateinit var talkJob: Job
    private var talkLoaded = false
    private lateinit var fragments: TalkFragments

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_talk)
        setSupportActionBar(talkToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        snackBarView = talkCoordinatorLayout

        val extras = intent.extras ?: return
        talkId = extras.getString("ID")
        userId = extras.getString("USER_ID")

        talkCardView.setOnClickListener {
            if (userId != null) {
                val intent = Intent(this, UserActivity::class.java)
                intent.putExtra("ID", userId)
                startActivity(intent)
            }
        }
    }

    @ExperimentalCoroutinesApi
    override fun onStart() {
        super.onStart()
        loadOrStartTalk()
    }

    @ExperimentalCoroutinesApi
    override fun onStop() {
        super.onStop()
        setOnNewMessageListener { true }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::talkJob.isInitialized) { talkJob.cancel() }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    /**
     * To load or start talk
     */
    @ExperimentalCoroutinesApi
    private fun loadOrStartTalk() {
        if (talkId != null) { loadTalk() }
        else if (userId != null) { startTalk() }
    }

    /**
     * Load talk if the talk ID is provided at the intent.
     */
    @ExperimentalCoroutinesApi
    private fun loadTalk() {
        val talkQueryChannel = Queries.getTalkQueryChannel(this, talkId.toString())

        talkJob = CoroutineScope(Dispatchers.IO).launch {
            try {
                talkQueryChannel.consumeEach {
                    val talk = it.data()?.talk

                    if (talk != null) {
                        val user = talk.talkers?.find { t -> t.user?.id != CurrentUser(this@TalkActivity).id }?.user
                        userId = user?.id

                        withContext(Dispatchers.Main) {
                            titleTextView.text = talk.name.toString()
                            AvatarLoader(user?.username.toString(), avatarImageView)

                            if (!talkLoaded) {
                                fragments = TalkFragments(talkId)

                                talkViewPager.adapter = TalkPagerAdapter(supportFragmentManager, lifecycle, fragments)

                                talkViewPager.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
                                    override fun onPageSelected(position: Int) {
                                        super.onPageSelected(position)
                                        hideKeyboard()
                                    }
                                })

                                TabLayoutMediator(talkTabs, talkViewPager) { tab, position ->
                                    tab.text = getString(fragments.getTitle(position))
                                }.attach()

                                // talkTabs.visibility = View.VISIBLE
                            }

                            setOnNewMessageListener { message ->
                                if (message.talk.id == talkId) {
                                    fragments.onNewMessage(message.toMessageFields())
                                    false
                                } else { true }
                            }

                            talkContainer.showContent()
                        }

                        fragments.onTalkChanged(talk)

                        talkLoaded = true
                    }
                    else {
                        withContext(Dispatchers.Main) {
                            Toast.makeText(this@TalkActivity, R.string.talk_not_found, Toast.LENGTH_LONG).show()
                            refreshConnectionStatus()
                            onSupportNavigateUp()
                        }
                    }
                }
            }
            catch (e: ApolloNetworkException) {
                handleApolloException(e)
                if (!talkLoaded) {
                    withContext(Dispatchers.Main) {
                        onSupportNavigateUp()
                    }
                }
            }
        }
    }

    /**
     * Start talk if the user ID is provided at the intent.
     */
    @ExperimentalCoroutinesApi
    private fun startTalk() {
        val startTalkDeferred = Mutations.getStarkTalkMutationAsync(this, userId.toString())

        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = startTalkDeferred.await().data()
                val startTalk = response?.startTalk

                withContext(Dispatchers.Main) {
                    if (startTalk?.success == true) {
                        talkId = startTalk.talk?.id
                        loadTalk()
                    }
                    else {
                        Toast.makeText(this@TalkActivity, startTalk?.message, Toast.LENGTH_LONG).show()
                        refreshConnectionStatus()
                        onSupportNavigateUp()
                    }
                }
            } catch (e: ApolloNetworkException) {
                handleApolloException(e)
                withContext(Dispatchers.Main) {
                    onSupportNavigateUp()
                }
            }
        }
    }

    /**
     * To hide the keyboard
     */
    private fun hideKeyboard() {
        var view = currentFocus
        if (view == null) { view = View(this) }
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}
