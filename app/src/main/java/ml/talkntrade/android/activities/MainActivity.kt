package ml.talkntrade.android.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Menu
import androidx.core.view.GravityCompat
import androidx.appcompat.app.ActionBarDrawerToggle
import android.view.MenuItem
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SearchView
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.ApolloCallback
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import ml.talkntrade.android.R
import ml.talkntrade.android.activities.shared.BaseActivity
import ml.talkntrade.android.data.CurrentUser
import ml.talkntrade.android.fragments.SearchFragment
import ml.talkntrade.android.fragments.TalksFragment
import ml.talkntrade.android.mutations.LogoutMutation
import ml.talkntrade.android.utils.AvatarLoader
import ml.talkntrade.android.utils.GraphQLClient
import ml.talkntrade.android.utils.ProgressBarHandler

class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {
    private lateinit var navView: NavigationView
    private lateinit var querySearchView: SearchView
    lateinit var progressBarHandler: ProgressBarHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(mainToolbar)

        snackBarView = findViewById(R.id.drawer_layout)


        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, mainToolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)

        loadNavViewData()

        if (savedInstanceState == null) {
            loadTalks()
        }

        progressBarHandler = ProgressBarHandler(this, mainContainer, mainProgressBar)
    }

    override fun onPause() {
        super.onPause()
        if (::querySearchView.isInitialized && !querySearchView.isIconified) {
            querySearchView.onActionViewCollapsed()
        }
    }

    override fun onRestart() {
        super.onRestart()
        loadTalks()
    }

    @ExperimentalCoroutinesApi
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search, menu)

        val queryItem = menu?.findItem(R.id.queryItem)
        querySearchView = queryItem?.actionView as SearchView
        querySearchView.queryHint = getString(R.string.search)
        querySearchView.setOnSearchClickListener { loadSearch() }
        querySearchView.setOnCloseListener {
            loadTalks()
            false
        }
        querySearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(query: String): Boolean {
                searchUsers(query)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                searchUsers(query)
                return false
            }

            private fun searchUsers(query: String) {
                val fragment = supportFragmentManager.findFragmentById(R.id.mainContainer) as SearchFragment
                fragment.searchUsers(query)
            }
        })

        return super.onCreateOptionsMenu(menu)
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        }
        else if (!querySearchView.isIconified) {
            querySearchView.onActionViewCollapsed()
            loadTalks()
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_wallet -> {
                val intent = Intent(this, WalletActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_logout -> {
                logout()
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun userDetails() {
        val intent = Intent(this, UserActivity::class.java)
        intent.putExtra("ID", CurrentUser(this).id)
        startActivity(intent)
    }

    private fun loadNavViewData() {
        navView.getHeaderView(0).apply {
            val username = CurrentUser(this@MainActivity).username.toString()
            usernameTextView.text =
                getString(R.string.at_username, username)
            AvatarLoader(username, navView.getHeaderView(0).avatarImageView)
            avatarImageView.setOnClickListener { userDetails() }
            usernameTextView.setOnClickListener { userDetails() }
        }
    }

    private fun loadTalks() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.mainContainer, TalksFragment.newInstance())
            .commitNowAllowingStateLoss()
    }

    private fun loadSearch() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.mainContainer, SearchFragment.newInstance())
            .commitNowAllowingStateLoss()
    }

    private fun logout() {
        val logoutDialog = AlertDialog.Builder(this)

        logoutDialog.setMessage(R.string.logout_confirmation)

        logoutDialog.setPositiveButton(android.R.string.ok) { _, _ ->
            Toast.makeText(this, "Logging out", Toast.LENGTH_LONG).show()

            val logoutMutation = LogoutMutation()

            val callback = ApolloCallback.wrap(object : ApolloCall.Callback<LogoutMutation.Data>() {
                override fun onResponse(response: Response<LogoutMutation.Data>) {
                    val logout = response.data()?.logout
                    Toast.makeText(this@MainActivity, logout?.message, Toast.LENGTH_LONG).show()
                    clearAndRedirectToLogin()
                }

                override fun onFailure(e: ApolloException) {
                    Toast.makeText(this@MainActivity, R.string.failed_to_logout, Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                    clearAndRedirectToLogin()
                }
            }, Handler(Looper.getMainLooper()))

            GraphQLClient(this).apolloClient.mutate(logoutMutation).enqueue(callback)
        }

        logoutDialog.setNegativeButton(android.R.string.cancel) { dialog, _ ->
            dialog.cancel()
        }

        logoutDialog.create().show()
    }
}
