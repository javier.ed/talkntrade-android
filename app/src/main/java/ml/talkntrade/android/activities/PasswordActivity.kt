package ml.talkntrade.android.activities

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.apollographql.apollo.coroutines.toDeferred
import com.apollographql.apollo.exception.ApolloNetworkException
import kotlinx.android.synthetic.main.activity_password.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ml.talkntrade.android.R
import ml.talkntrade.android.activities.shared.BaseActivity
import ml.talkntrade.android.mutations.ChangePasswordMutation
import ml.talkntrade.android.type.PasswordInput
import ml.talkntrade.android.utils.GraphQLClient
import ml.talkntrade.android.utils.ProgressBarHandler
import ml.talkntrade.android.utils.disable
import ml.talkntrade.android.utils.enable

class PasswordActivity : BaseActivity() {
    private lateinit var progressBarHandler: ProgressBarHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        snackBarView = passwordCoordinatorLayout

        progressBarHandler = ProgressBarHandler(this, PasswordLinealLayout, PasswordProgressBar)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.save, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.saveItem) {
            item.disable()
            progressBarHandler.show()

            val passwordInput = PasswordInput(newPasswordEditText.text.toString(), passwordConfirmationEditText.text.toString())
            val changePasswordMutation = ChangePasswordMutation(currentPasswordEditText.text.toString(), passwordInput)

            val changePasswordDeferred = GraphQLClient(this).apolloClient.mutate(changePasswordMutation).toDeferred()

            CoroutineScope(Dispatchers.IO).launch {
                try {
                    val response = changePasswordDeferred.await()
                    val changePassword = response.data()?.changePassword

                    withContext(Dispatchers.Main) {
                        if (changePassword?.success == true) {
                            Toast.makeText(
                                this@PasswordActivity,
                                changePassword.message,
                                Toast.LENGTH_LONG
                            ).show()
                            finish()
                            onSupportNavigateUp()
                        } else {
                            Toast.makeText(
                                this@PasswordActivity,
                                changePassword?.message,
                                Toast.LENGTH_LONG
                            ).show()

                            changePassword?.errors?.forEach {
                                if (it.path?.get(0) == "password") {
                                    currentPasswordEditText.error = it.message
                                } else if (it.path?.get(0) == "attributes") {
                                    when (it.path[1]) {
                                        "password" -> newPasswordEditText.error = it.message
                                        "passwordConfirmation" -> passwordConfirmationEditText.error =
                                            it.message
                                    }
                                }
                            }
                            progressBarHandler.hide()
                            item.enable()
                        }
                    }
                }
                catch(e: ApolloNetworkException) {
                    handleApolloException(e)
                    withContext(Dispatchers.Main) {
                        progressBarHandler.hide()
                        item.enable()
                    }
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
