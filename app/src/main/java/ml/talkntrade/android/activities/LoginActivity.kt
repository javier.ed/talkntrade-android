package ml.talkntrade.android.activities

import android.content.Intent
import android.os.*
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.MotionEvent
import androidx.appcompat.app.AppCompatActivity
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import com.apollographql.apollo.coroutines.toDeferred
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.security.Keys
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ml.talkntrade.android.BuildConfig
import ml.talkntrade.android.mutations.RegisterDeviceMutation
import ml.talkntrade.android.R
import ml.talkntrade.android.TalkntradeApplication
import ml.talkntrade.android.data.CurrentDevice
import ml.talkntrade.android.data.CurrentSession
import ml.talkntrade.android.data.CurrentUser
import ml.talkntrade.android.mutations.LoginMutation
import ml.talkntrade.android.utils.GraphQLClient
import ml.talkntrade.android.utils.ProgressBarHandler

class LoginActivity : AppCompatActivity() {
    lateinit var progressBarHandler: ProgressBarHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        progressBarHandler = ProgressBarHandler(this, loginLayout, loginProgressBar)

        findOrRegisterDevice()


        if(CurrentSession(this).id?.isNotBlank() == true) {
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            finish()
        }

        passwordEditText.setOnEditorActionListener(TextView.OnEditorActionListener { _, id, _ ->
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptLogin()
                return@OnEditorActionListener true
            }
            false
        })

        passwordEditText.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_DOWN && event.rawX >= (passwordEditText.right - passwordEditText.compoundDrawables[2].bounds.width())) {
                if (passwordEditText.transformationMethod == HideReturnsTransformationMethod.getInstance()) {
                    passwordEditText.transformationMethod = PasswordTransformationMethod.getInstance()
                    passwordEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eye_close_24dp, 0)
                }
                else {
                    passwordEditText.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    passwordEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eye_open_24dp, 0)
                }
                true
            }
            else { false }
        }

        loginButton.setOnClickListener { attemptLogin() }

        goToResetPasswordButton.setOnClickListener {
            val intent = Intent(this, ResetPasswordActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            finish()
        }

        goToRegisterButton.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            finish()
        }
    }

    private fun findOrRegisterDevice() {
        if (CurrentDevice(this).id?.isNotBlank() == true) {
            progressBarHandler.hide()
            return
        }

        Toast.makeText(this, R.string.registering_device, Toast.LENGTH_LONG).show()

        val key = Keys.hmacShaKeyFor(BuildConfig.APP_SECRET_KEY.toByteArray())
        val appToken = Jwts.builder().setPayload("{}").signWith(key).compact()
        val registerDeviceMutation = RegisterDeviceMutation(BuildConfig.APP_ID, appToken)
        val registerDeviceDeferred = GraphQLClient(this).apolloClient.mutate(registerDeviceMutation).toDeferred()

        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = registerDeviceDeferred.await().data()
                val registerDevice = response?.registerDevice

                withContext(Dispatchers.Main) {
                    if (registerDevice?.success == true) {
                        CurrentDevice(this@LoginActivity).update(registerDevice.deviceId, registerDevice.deviceSecretKey)
                        Toast.makeText(this@LoginActivity, registerDevice.message, Toast.LENGTH_LONG).show()
                        progressBarHandler.hide()
                    } else {
                        Toast.makeText(
                            this@LoginActivity,
                            registerDevice?.message,
                            Toast.LENGTH_LONG
                        ).show()
                        finish()
                    }
                }
            }
            catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    GraphQLClient.handleError(this@LoginActivity, e)
                    finish()
                }
            }
        }
    }

    private fun attemptLogin() {
        progressBarHandler.show()

        val loginMutation = LoginMutation(loginEditText.text.toString(), passwordEditText.text.toString())
        val loginDeferred = GraphQLClient(this).apolloClient.mutate(loginMutation).toDeferred()

        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = loginDeferred.await().data()
                val login = response?.login

                withContext(Dispatchers.Main) {
                    if (login?.success == true) {
                        CurrentUser(this@LoginActivity).update(login.user?.id, login.user?.username)
                        CurrentSession(this@LoginActivity).update(login.sessionId)
                        Toast.makeText(applicationContext, login.message, Toast.LENGTH_LONG).show()
                        val intent = Intent(applicationContext, MainActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        startActivity(intent)
                        finish()
                    }
                    else {
                        Toast.makeText(this@LoginActivity, login?.message, Toast.LENGTH_LONG).show()
                        progressBarHandler.hide()
                    }
                }
            }
            catch(e: Exception) {
                withContext(Dispatchers.Main) {
                    GraphQLClient.handleError(this@LoginActivity, e)
                    progressBarHandler.hide()
                }
            }
        }
    }
}
