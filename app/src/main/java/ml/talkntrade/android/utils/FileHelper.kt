package ml.talkntrade.android.utils

import android.content.Context
import android.net.Uri
import android.webkit.MimeTypeMap
import android.widget.ImageView
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso

class FileHelper {
    companion object {
        fun getFileType(context: Context, uri: Uri) : String? {
            return context.contentResolver.getType(uri)
                ?: MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(uri.toString()))
        }

        fun loadImage(context: Context, uri: Uri, imageView: ImageView) {
            Picasso.Builder(context)
                .downloader(OkHttp3Downloader(GraphQLClient(context).getOkHttpClient()))
                .build()
                .load(uri)
                .into(imageView)
        }
    }
}
