package ml.talkntrade.android.utils

import android.content.Context
import com.apollographql.apollo.api.Operation
import com.vinted.actioncable.client.kotlin.*
import ml.talkntrade.android.BuildConfig
import ml.talkntrade.android.data.CurrentDevice
import ml.talkntrade.android.data.CurrentSession
import java.io.IOException
import java.net.URI


/**
 * To handle GraphQL subscriptions
 */
class SubscriptionsHandler(val context: Context) {
    private lateinit var actionCableConsumer : Consumer
    private val actionCableSubscription : Subscription by lazy {
        val options = Consumer.Options()
        val currentDevice = CurrentDevice(context)
        val deviceId = currentDevice.id
        val deviceToken = currentDevice.token

        options.connection.headers = if(deviceId?.isNotBlank() == true && deviceToken?.isNotBlank() == true) {
            mapOf(
                "Accept-Language" to Locales(context).get(),
                "X-Device-Id" to deviceId.toString(),
                "X-Device-Token" to deviceToken.toString()
            )
        }
        else {
            mapOf("Accept-Language" to Locales(context).get())
        }

        options.connection.reconnection = true
        options.connection.reconnectionMaxAttempts = 1000000
        options.connection.reconnectionDelay = 5
        options.connection.reconnectionDelayMax = 5

        actionCableConsumer = ActionCable.createConsumer(URI(BuildConfig.WEBSOCKET_URL), options)

        val graphqlChannel = Channel("GraphqlChannel", mapOf("channel_id" to CurrentSession(context).id))
        actionCableConsumer.subscriptions.create(graphqlChannel)
    }

    fun connect() {
        if (::actionCableConsumer.isInitialized) actionCableConsumer.connect()
    }

    fun disconnect() {
        if (::actionCableConsumer.isInitialized) actionCableConsumer.disconnect()
    }

    /**
     * Callback called when the subscription has been successfully completed.
     */
    var onConnected: ConnectedHandler? = null
        get() = actionCableSubscription.onConnected
        set(value) {
            actionCableSubscription.onConnected = value
            field = value
        }

    /**
     * Callback called when the subscription is rejected by the server.
     */
    var onRejected: RejectedHandler? = null

    /**
     * Callback called when the subscription receives data from the server.
     */
    var onReceived : ReceivedHandler? = null
        get() = actionCableSubscription.onReceived
        set(value) {
            actionCableSubscription.onReceived = value
            field = value
        }

    /**
     * Callback called when the subscription has been closed.
     */
    var onDisconnected: DisconnectedHandler? = null
        get() = actionCableSubscription.onDisconnected
        set(value) {
            actionCableSubscription.onDisconnected = value
            field = value
        }

    /**
     * Callback called when the subscription encounters any error.
     */
    var onFailed: FailedHandler? = null
        get() = actionCableSubscription.onFailed
        set(value) {
            actionCableSubscription.onFailed = value
            field = value
        }

    fun subscribe(operation: Operation<*, *, *>) {
        actionCableSubscription.perform("execute", requestBody(operation))
    }

    @Throws(IOException::class)
    private fun requestBody(operation: Operation<*, *, *>): Map<String, Any> {
        return mapOf(
            "operationName" to operation.name().name(),
            "variables" to mapOf<String, Any>(),
            "extensions" to mapOf<String, Any>(
                "persistedQuery" to mapOf(
                    "version" to 1,
                    "sha256Hash" to operation.operationId()
                )
            ),
            "query" to operation.queryDocument().replace("\\n".toRegex(), "")
        )
    }
}
