package ml.talkntrade.android.utils

import android.content.Context
import com.apollographql.apollo.api.Input
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.coroutines.toChannel
import com.apollographql.apollo.coroutines.toDeferred
import com.apollographql.apollo.fetcher.ApolloResponseFetchers
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.Channel
import ml.talkntrade.android.queries.*

/**
 * Object with queries
 */
@ExperimentalCoroutinesApi
object Queries {

    /**
     * Get a talk query channel
     *
     * @param [id] Talk ID
     */
    fun getTalkQueryChannel(context: Context, id: String) : Channel<Response<TalkQuery.Data>> {
        val talkQuery = TalkQuery(id, Input.absent(), Input.optional(15), Input.optional(true))
        return GraphQLClient(context).apolloClientWithCache.query(talkQuery).toChannel()
    }

    /**
     * Gets a query channel for messages in a chat
     *
     * @param [id] Talk ID
     * @param [beforeId] Get a list messages before an ID
     */
    fun getTalkMessagesQueryChannel(context: Context, id: String, beforeId : String? = null) : Channel<Response<TalkMessagesQuery.Data>> {
        var beforeMessageId = Input.absent<String>()
        var limit = Input.optional(15)
        var includeUnseen = Input.optional(true)

        if (beforeId != null) {
            beforeMessageId = Input.optional(beforeId)
            includeUnseen = Input.absent<Boolean>()
        }

        val talkMessagesQuery = TalkMessagesQuery(id, beforeMessageId, limit, includeUnseen)
        return GraphQLClient(context).apolloClientWithCache.query(talkMessagesQuery).toChannel()
    }

    /**
     * Get current user's assets query channel
     */
    fun getAssetsQueryChannel(context: Context): Channel<Response<AssetsQuery.Data>> {
        val assetsQuery = AssetsQuery()
        return GraphQLClient(context).apolloClientWithCache.query(assetsQuery).toChannel()
    }

    /**
     * Get info query
     */
    fun getInfoQueryAsync(context: Context) : Deferred<Response<InfoQuery.Data>> {
        val infoQuery = InfoQuery()
        return GraphQLClient(context).apolloClient.query(infoQuery).responseFetcher(ApolloResponseFetchers.NETWORK_ONLY).toDeferred()
    }
}
