package ml.talkntrade.android.utils

import android.graphics.Color
import android.widget.ImageView
import com.makeramen.roundedimageview.RoundedTransformationBuilder
import com.squareup.picasso.Picasso

class AvatarLoader(name: String, imageView: ImageView) {
    init {
        val imageUrl = "https://robohash.org/$name?set=set4"

        val transformation = RoundedTransformationBuilder().borderColor(Color.LTGRAY)
            .borderWidthDp(3f).cornerRadiusDp(100f).oval(false).build()

        Picasso.get().load(imageUrl).transform(transformation).into(imageView)
    }
}
