package ml.talkntrade.android.utils

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.view.View
import android.widget.ProgressBar

class ProgressBarHandler(val context: Context, val view: View, val progressBar: ProgressBar) {
    private val shortAnimTime = context.resources.getInteger(android.R.integer.config_shortAnimTime).toLong()

    fun toggle(show: Boolean = (progressBar.visibility == View.GONE)) {
        view.visibility = if (show) View.GONE else View.VISIBLE
        view.animate()
            .setDuration(shortAnimTime)
            .alpha((if (show) 0 else 1).toFloat())
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    view.visibility = if (show) View.GONE else View.VISIBLE
                }
            })

        progressBar.visibility = if (show) View.VISIBLE else View.GONE
        progressBar.animate()
            .setDuration(shortAnimTime)
            .alpha((if (show) 1 else 0).toFloat())
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    progressBar.visibility = if (show) View.VISIBLE else View.GONE
                }
            })
    }

    fun show() {
        toggle(true)
    }

    fun hide() {
        toggle(false)
    }
}
