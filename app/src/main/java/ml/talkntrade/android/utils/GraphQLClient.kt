package ml.talkntrade.android.utils

import android.content.Context
import android.widget.Toast
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.Operation
import com.apollographql.apollo.api.ResponseField
import com.apollographql.apollo.cache.normalized.CacheKey
import com.apollographql.apollo.cache.normalized.CacheKeyResolver
import com.apollographql.apollo.cache.normalized.lru.EvictionPolicy
import com.apollographql.apollo.cache.normalized.lru.LruNormalizedCacheFactory
import com.apollographql.apollo.cache.normalized.sql.ApolloSqlHelper
import com.apollographql.apollo.cache.normalized.sql.SqlNormalizedCacheFactory
import com.apollographql.apollo.fetcher.ApolloResponseFetchers
import com.apollographql.apollo.response.CustomTypeAdapter
import com.apollographql.apollo.response.CustomTypeValue
import ml.talkntrade.android.BuildConfig
import ml.talkntrade.android.R
import ml.talkntrade.android.data.CurrentDevice
import ml.talkntrade.android.data.CurrentSession
import ml.talkntrade.android.type.CustomType
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import java.util.*
import java.util.concurrent.TimeUnit

class GraphQLClient(val context: Context) {
    companion object {
        fun handleError(context: Context?, e: Exception?) {
            if (context != null) {
                Toast.makeText(context, R.string.failed_to_execute_request, Toast.LENGTH_LONG).show()
            }
            e?.printStackTrace()
        }
    }

    private val dateCustomAdapter = object : CustomTypeAdapter<Date> {
        override fun decode(value: CustomTypeValue<*>): Date {
            return DateHelper().parse(value.value.toString()) as Date
        }

        override fun encode(value: Date): CustomTypeValue<*> {
            return CustomTypeValue.GraphQLString(DateHelper.stringify(value))
        }
    }

    val apolloClient: ApolloClient by lazy {
        ApolloClient.builder()
            .serverUrl(BuildConfig.SERVER_URL)
            .okHttpClient(getOkHttpClient())
            .addCustomTypeAdapter(CustomType.ISO8601DATETIME, dateCustomAdapter)
            .build()
    }

    val apolloClientWithCache: ApolloClient by lazy {
        val apolloSqlHelper = ApolloSqlHelper.create(context, "apollo_cache_session_${CurrentSession(context).id}")
        val sqlNormalizedCacheFactory = SqlNormalizedCacheFactory(apolloSqlHelper)
        val cacheKeyResolver = object : CacheKeyResolver() {
            override fun fromFieldRecordSet(field: ResponseField, recordSet: Map<String, Any>): CacheKey {
                return formatCacheKey(recordSet["id"] as String?)
            }

            override fun fromFieldArguments(field: ResponseField, variables: Operation.Variables): CacheKey {
                return formatCacheKey(field.resolveArgument("id", variables) as String?)
            }

            private fun formatCacheKey(id: String?) : CacheKey {
                return if (id?.isNotEmpty() == true) {
                    CacheKey.from(id)
                } else {
                    CacheKey.NO_KEY
                }
            }
        }

        val memoryFirstThenSqlCacheFactory = LruNormalizedCacheFactory(
            EvictionPolicy.builder().maxSizeBytes((10 * 1024).toLong()).build()
        ).chain(sqlNormalizedCacheFactory)

        ApolloClient.builder()
            .serverUrl(BuildConfig.SERVER_URL)
            .okHttpClient(getOkHttpClient())
            .normalizedCache(memoryFirstThenSqlCacheFactory, cacheKeyResolver)
            .addCustomTypeAdapter(CustomType.ISO8601DATETIME, dateCustomAdapter)
            .defaultResponseFetcher(ApolloResponseFetchers.CACHE_AND_NETWORK)
            .build()
    }

    private fun getRequest(chain: Interceptor.Chain) : Request {
        val chainRequestBuilder = chain.request().newBuilder()
        val currentDevice = CurrentDevice(context)
        val deviceId = currentDevice.id
        val deviceToken = currentDevice.token

        if(deviceId?.isNotBlank() == true && deviceToken?.isNotBlank() == true) {
            chainRequestBuilder.addHeader("X-Device-Id", deviceId.toString())
                               .addHeader("X-Device-Token", deviceToken.toString())
        }

        return chainRequestBuilder.addHeader("Accept-Language", Locales(context).get()).build()
    }

    fun getOkHttpClient() : OkHttpClient = OkHttpClient.Builder()
        .connectTimeout(1, TimeUnit.HOURS)
        .readTimeout(1, TimeUnit.HOURS)
        .writeTimeout(1, TimeUnit.HOURS)
        .addNetworkInterceptor { chain -> chain.proceed(getRequest(chain)) }
        .build()
}
