package ml.talkntrade.android.utils

import android.content.Context
import android.os.Build

class Locales(val context: Context) {
    fun get() : String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context.resources.configuration.locales.toLanguageTags()
        }
        else {
            context.resources.configuration.locale.toString().replace("_", "-")
        }
    }
}
