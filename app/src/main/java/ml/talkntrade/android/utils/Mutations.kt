package ml.talkntrade.android.utils

import android.content.Context
import com.apollographql.apollo.api.Input
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.coroutines.toDeferred
import kotlinx.coroutines.Deferred
import ml.talkntrade.android.mutations.*
import ml.talkntrade.android.type.MessageAttachableInput
import ml.talkntrade.android.type.MessageInput

/**
 * Object with mutations
 */
object Mutations {

    /**
     * Get a mutation to start a talk
     *
     * @param [userId] Other user's ID
     */
    fun getStarkTalkMutationAsync(context: Context, userId: String) : Deferred<Response<StartTalkMutation.Data>> {
        val startTalkMutation = StartTalkMutation(userId)
        return GraphQLClient(context).apolloClient.mutate(startTalkMutation).toDeferred()
    }

    /**
     * Get a mutation to mark a message and the previous ones as seen
     *
     * @param [id] Message ID
     */
    fun getMarkSeenMessageMutationAsync(context: Context, id: String) : Deferred<Response<MarkSeenMessageMutation.Data>> {
        val markSeenMessageMutation = MarkSeenMessageMutation(id)
        return GraphQLClient(context).apolloClient.mutate(markSeenMessageMutation).toDeferred()
    }

    /**
     * Get a mutation to send a message
     *
     * @param [talkId] Talk ID
     * @param [content] Content of the message
     * @param [amount] (Optional) if it's sending money
     */
    fun getSendMessageMutationAsync(context: Context, talkId: String, content: String, amount: Double? = null) : Deferred<Response<SendMessageMutation.Data>> {
        var attachable = Input.absent<MessageAttachableInput>()

        if (amount != null) {
            attachable = Input.optional(MessageAttachableInput(Input.optional("transaction"), Input.optional(amount), Input.optional("BTC")))
        }

        val attributes = MessageInput(content, attachable, talkId)
        val sendMessageMutation = SendMessageMutation(attributes)
        return GraphQLClient(context).apolloClient.mutate(sendMessageMutation).toDeferred()
    }
}
