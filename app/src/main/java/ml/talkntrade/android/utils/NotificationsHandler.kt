package ml.talkntrade.android.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.os.Build
import android.provider.Settings
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import ml.talkntrade.android.BuildConfig
import ml.talkntrade.android.R

class NotificationsHandler(
    private val context: Context,
    private val id: String = BuildConfig.APPLICATION_ID + ".notifications",
    name: String = "Notifications"
) {
    private var counter = 0

    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(id, name, importance).apply {
                enableVibration(true)
            }

            val notificationManager: NotificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    fun notify(title: String, content: String, pendingIntent: PendingIntent) {
        val notificationBuilder = NotificationCompat.Builder(context, id)
            .setSmallIcon(R.drawable.ic_notification_icon)
            .setContentTitle(title)
            .setContentText(content)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setGroup(id)
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)
            .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)

        with(NotificationManagerCompat.from(context)) {
            notify(counter, notificationBuilder.build())
        }

        counter++
    }
}
