package ml.talkntrade.android.utils

import android.view.MenuItem

fun MenuItem.disable() {
    isEnabled = false
    if (icon != null) { icon.alpha = 130 }
}

fun MenuItem.enable() {
    if (icon != null) { icon.alpha = 255 }
    isEnabled = true
}
