package ml.talkntrade.android.adapters

import android.content.Intent
import android.graphics.Paint
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_transaction.view.*
import ml.talkntrade.android.R
import ml.talkntrade.android.queries.AssetsQuery

class TransactionsAdapter(private val transactions: List<AssetsQuery.Transaction?>, private val currency: AssetsQuery.Currency?)
    : RecyclerView.Adapter<TransactionsAdapter.TransactionsViewHolder>() {
    class TransactionsViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionsViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_transaction, parent, false)
        return TransactionsViewHolder(v)
    }

    override fun onBindViewHolder(holder: TransactionsViewHolder, position: Int) {
        val transaction = transactions[position]

        holder.view.numberTextView.text = holder.view.context.getString(R.string.number_format, transaction?.number)
        holder.view.createdAtTextView.text = transaction?.createdAt.toString()

        if (transaction?.txid != null && transaction.txUrl != null) {
            holder.view.txidTextView.paintFlags = holder.view.txidTextView.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            holder.view.txidTextView.text = transaction.txid
            holder.view.txidTextView.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(transaction.txUrl))
                holder.view.context.startActivity(intent)
            }
            holder.view.txidLayout.visibility = View.VISIBLE
        }
        else {
            holder.view.txidLayout.visibility = View.GONE
        }


        val currencyString = holder.view.context.getString(
            R.string.amount_iso_code_format,
            currency?.exponent, currency?.isoCode
        )

        if (transaction?.received == true) {
            holder.view.amountTextView.text = currencyString.format(transaction.amount)
            when (transaction.category) {
                "transfer" -> holder.view.categoryTextView.setText(R.string.transfer_received)
                "deposit" -> {
                    holder.view.categoryTextView.setText(R.string.deposit)
                    holder.view.feeTextView.text = holder.view.context.getString(
                        R.string.minus_amount,
                        currencyString.format(transaction.fee)
                    )
                }
            }
        }
        else {
            holder.view.amountTextView.text = holder.view.context.getString(
                R.string.minus_amount,
                currencyString.format(transaction?.amount)
            )
            holder.view.feeTextView.text = holder.view.context.getString(
                R.string.minus_amount,
                currencyString.format(transaction?.fee)
            )
            when (transaction?.category) {
                "transfer" -> holder.view.categoryTextView.setText(R.string.transfer_sent)
                "withdrawal" -> holder.view.categoryTextView.setText(R.string.withdrawal)
            }
        }
    }

    override fun getItemCount() = transactions.size
}
