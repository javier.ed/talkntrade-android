package ml.talkntrade.android.adapters

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import ml.talkntrade.android.collections.TalkFragments


class TalkPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle, private val fragments: TalkFragments) :
    FragmentStateAdapter(fragmentManager, lifecycle) {
    override fun createFragment(position: Int) = fragments.getFragment(position)

    override fun getItemCount() = fragments.count()
}
