package ml.talkntrade.android.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_user.view.*
import ml.talkntrade.android.R
import ml.talkntrade.android.activities.TalkActivity
import ml.talkntrade.android.queries.UsersQuery.User
import ml.talkntrade.android.utils.AvatarLoader

class UsersAdapter(private val usersDataset: List<User?>) : RecyclerView.Adapter<UsersAdapter.UsersViewHolder>() {
    class UsersViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
        return UsersViewHolder(v)
    }

    override fun onBindViewHolder(holder: UsersViewHolder, position: Int) {
        val user = usersDataset[position]

        if (user != null) {
            holder.view.displayNameTextView.text = user.profile?.displayName
            holder.view.usernameTextView.text = holder.view.context.getString(R.string.at_username, user.username)

            AvatarLoader(user.username.toString(), holder.view.avatarImageView)

            holder.view.setOnClickListener {
                val intent = Intent(it.context, TalkActivity::class.java)
                intent.putExtra("USER_ID", user.id)
                it.context.startActivity(intent)
            }
        }
    }

    override fun getItemCount() = usersDataset.size
}
