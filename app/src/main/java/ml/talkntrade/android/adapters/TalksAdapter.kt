package ml.talkntrade.android.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_talk.view.*
import ml.talkntrade.android.R
import ml.talkntrade.android.activities.TalkActivity
import ml.talkntrade.android.data.CurrentUser
import ml.talkntrade.android.queries.TalksQuery.Talk
import ml.talkntrade.android.utils.AvatarLoader
import ml.talkntrade.android.utils.DateHelper

class TalksAdapter(private val talks: List<Talk?>) : RecyclerView.Adapter<TalksAdapter.TalksViewHolder>() {
    class TalksViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TalksViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_talk, parent, false)
        return TalksViewHolder(v)
    }

    override fun onBindViewHolder(holder: TalksViewHolder, position: Int) {
        val talk = talks[position]

        val user = talk?.talkers?.find { t -> t.user?.id != CurrentUser(holder.view.context).id }?.user

        holder.view.itemTalkName.text = talk?.name

        AvatarLoader(user?.username.toString(), holder.view.itemTalkAvatar)

        val lastMessage = talk?.messages?.get(0)

        if (lastMessage != null) {
            holder.view.lastMessageAtTextView.text = DateHelper().timeAgo(talk.lastMessageAt?.time as Long)
             holder.view.youTextView.visibility = if (lastMessage.received == true) View.GONE else View.VISIBLE
            holder.view.lastMessageTextView.text = lastMessage.content
        }

        if (talk?.unseenCount as Int > 0) {
            holder.view.unseenCountTextView.visibility = View.VISIBLE
            holder.view.unseenCountTextView.text = talk.unseenCount.toString()
        }
        else {
            holder.view.unseenCountTextView.visibility = View.GONE
        }

        holder.view.talkCardView.setOnClickListener {
            val intent = Intent(holder.view.context, TalkActivity::class.java)
            intent.putExtra("ID", talk.id)
            holder.view.context.startActivity(intent)
        }
    }

    override fun getItemCount() = talks.size
}
