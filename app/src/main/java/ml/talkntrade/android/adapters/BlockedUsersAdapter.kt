package ml.talkntrade.android.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.PopupMenu
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.apollographql.apollo.coroutines.toDeferred
import com.apollographql.apollo.exception.ApolloNetworkException
import kotlinx.android.synthetic.main.item_blocked_user.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ml.talkntrade.android.R
import ml.talkntrade.android.activities.BlockedUsersActivity
import ml.talkntrade.android.mutations.UnblockUserMutation
import ml.talkntrade.android.queries.CurrentUserBlockedUsersQuery
import ml.talkntrade.android.utils.AvatarLoader
import ml.talkntrade.android.utils.GraphQLClient

class BlockedUsersAdapter(
    private val blockedUsers: MutableList<CurrentUserBlockedUsersQuery.BlockedUser>,
    private val activity: BlockedUsersActivity
)
    : RecyclerView.Adapter<BlockedUsersAdapter.BlockedUsersViewHolder>() {
    class BlockedUsersViewHolder(val view: LinearLayout) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BlockedUsersViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_blocked_user, parent, false) as LinearLayout
        return BlockedUsersViewHolder(v)
    }

    override fun onBindViewHolder(holder: BlockedUsersViewHolder, position: Int) {
        val blockedUser = blockedUsers[position]

        AvatarLoader(blockedUser.blockedUser?.username.toString(), holder.view.avatarImageView)

        holder.view.displayNameTextView.text = blockedUser.blockedUser?.profile?.displayName
        holder.view.usernameTextView.text = blockedUser.blockedUser?.username

        holder.view.editBlockedUserButton.setOnClickListener {
            val popupMenu = PopupMenu(holder.view.context, holder.view.editBlockedUserButton)

            popupMenu.inflate(R.menu.blocked_user)

            popupMenu.setOnMenuItemClickListener {
                if (it.itemId == R.id.unblockUserItem) {
                    val unblockUserMutation = UnblockUserMutation(blockedUser.blockedUser?.id.toString())
                    val unblockUserDeferred =
                        GraphQLClient(activity).apolloClient.mutate(unblockUserMutation).toDeferred()

                    CoroutineScope(Dispatchers.IO).launch {
                        try {
                            val response = unblockUserDeferred.await().data()
                            val unblockUser = response?.unblockUser

                            withContext(Dispatchers.Main) {
                                if (unblockUser?.success == true) {
                                    Toast.makeText(activity, unblockUser.message, Toast.LENGTH_LONG).show()
                                    blockedUsers.removeAt(position)
                                    notifyDataSetChanged()
                                } else {
                                    Toast.makeText(activity, unblockUser?.message, Toast.LENGTH_LONG).show()
                                }
                            }
                        }
                        catch (e: ApolloNetworkException) {
                            activity.handleApolloException(e)
                        }
                    }
                }
                true
            }

            popupMenu.show()
        }
    }

    override fun getItemCount() = blockedUsers.size
}
