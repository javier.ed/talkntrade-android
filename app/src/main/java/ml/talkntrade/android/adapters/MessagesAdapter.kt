package ml.talkntrade.android.adapters

import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_message.view.*
import ml.talkntrade.android.R
import ml.talkntrade.android.collections.Messages
import ml.talkntrade.android.utils.DateHelper

class MessagesAdapter(private val messages: Messages) : RecyclerView.Adapter<MessagesAdapter.MessagesViewHolder>() {
    class MessagesViewHolder(val view: LinearLayout) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessagesViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_message, parent, false) as LinearLayout
        val holder = MessagesViewHolder(v)
        return holder
    }

    override fun onBindViewHolder(holder: MessagesViewHolder, position: Int) {
        messages.getAt(position)?.let { message ->
            holder.view.messageContent.text = message.content

            holder.view.timeAgoTextView.text = if (message.createdAt != null) {
                val time = message.createdAt.time
                DateHelper().timeAgo(time)
            } else {
                holder.view.context.getString(R.string.sending)
            }

            when {
                message.attachable?.asTransaction != null -> {
                    val currency = message.attachable.asTransaction.currency
                    val amount = message.attachable.asTransaction.amount
                    holder.view.amountTextView.text =
                        holder.view.context.getString(
                            R.string.amount_iso_code_format,
                            currency?.exponent,
                            currency?.isoCode
                        ).format(amount)

                    holder.view.sentOrReceivedTextView.setText(if (message.attachable.asTransaction.received == true) R.string.received else R.string.sent)

                    holder.view.sendMoneyLayout.visibility = View.VISIBLE
                }
            }


            val params = holder.view.layoutParams as RecyclerView.LayoutParams

            if (message.received == true) {
                params.rightMargin = 50
                holder.view.gravity = Gravity.START
            } else {
                params.leftMargin = 50
            }

            holder.view.layoutParams = params

            holder.setIsRecyclable(false)
        }
    }

    override fun getItemCount() = messages.count()
}
