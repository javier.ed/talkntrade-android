package ml.talkntrade.android

import android.app.Application

class TalkntradeApplication : Application() {
    companion object {
        val productUnits = arrayOf("none", "g", "kg", "oz", "lb", "cm", "m", "in", "ft")
    }
}
