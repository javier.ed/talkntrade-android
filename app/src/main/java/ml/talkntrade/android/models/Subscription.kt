package ml.talkntrade.android.models

import ml.talkntrade.android.fragment.MessageFields
import ml.talkntrade.android.utils.DateHelper

data class Subscription(val result: SubscriptionResult?) {
    val newMessage: NewMessage?
        get() = result?.data?.newMessage
}

data class SubscriptionResult(val data: SubscriptionData?)

data class SubscriptionData(val newMessage: NewMessage?)

data class NewMessage(
    val id: String,
    val createdAt: String,
    val content: String,
    val received: Boolean,
    val seen: Boolean,
    val talk: Talk,
    val attachable: Attachable?
) {
    fun toMessageFields(): MessageFields {
        val attachable = if (attachable != null) {
            when (attachable.__typename) {
                "Transaction" -> {
                    val currency = MessageFields.Currency(
                        "Currency",
                        attachable.currency.id,
                        attachable.currency.isoCode,
                        attachable.currency.exponent
                    )
                    val amount = attachable.amount
                    val received = attachable.received

                    MessageFields.Attachable(
                        "Attachable",
                        MessageFields.AsTransaction("Transaction", attachable.id, amount, currency, received)
                    )
                }
                else -> null
            }
        } else {
            null
        }

        return MessageFields(
            "MessageFields",
            id,
            DateHelper().parse(createdAt),
            content,
            attachable,
            received,
            seen
        )
    }
}

data class Attachable(
    val __typename: String, val id: String, val currency: Currency, val amount: Double, val received: Boolean
)

data class Currency(val id: String, val isoCode: String, val exponent: Int)

data class Talk(val id: String, val name: String)
