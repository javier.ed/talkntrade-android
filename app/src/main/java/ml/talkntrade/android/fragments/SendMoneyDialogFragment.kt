package ml.talkntrade.android.fragments

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.DialogFragment
import com.apollographql.apollo.exception.ApolloNetworkException
import kotlinx.android.synthetic.main.dialog_fragment_send_money.view.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.consumeEach
import ml.talkntrade.android.R
import ml.talkntrade.android.activities.TalkActivity
import ml.talkntrade.android.utils.Mutations
import ml.talkntrade.android.utils.Queries

/**
 * Dialog fragment to send money
 */
class SendMoneyDialogFragment : DialogFragment() {
    private var talkId: String? = null
    private lateinit var assetJob: Job
    private lateinit var sendMoneyDialogView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { talkId = it.getString(ChatFragment.TALK_ID) as String }
        isCancelable = false
    }

    @ExperimentalCoroutinesApi
    @SuppressLint("InflateParams")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        sendMoneyDialogView = requireActivity().layoutInflater.inflate(R.layout.dialog_fragment_send_money, null)
        val sendMoneyDialog = AlertDialog.Builder(activity).setView(sendMoneyDialogView).setTitle(R.string.send_money)
            .setCancelable(false)
            .setPositiveButton(android.R.string.ok, null)
            .setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.dismiss() }
            .create()

        sendMoneyDialog.setOnShowListener {
            loadAsset()

            val okButton = sendMoneyDialog.getButton(androidx.appcompat.app.AlertDialog.BUTTON_POSITIVE)
            val cancelButton = sendMoneyDialog.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE)
            okButton.setOnClickListener {
                okButton.isEnabled = false
                cancelButton.isEnabled = false

                val content = sendMoneyDialogView.messageContentEditText.text.toString()
                val amount = sendMoneyDialogView.amountEditText.text.toString().toDouble()
                val sendMessageDeferred = Mutations.getSendMessageMutationAsync(context as Context, talkId.toString(), content, amount)

                CoroutineScope(Dispatchers.IO).launch {
                    try {
                        val response = sendMessageDeferred.await().data()
                        val sendMessage = response?.sendMessage

                        withContext(Dispatchers.Main) {
                            if (sendMessage?.success == true) {
                                sendMoneyDialog.dismiss()
                            }
                            else {
                                Toast.makeText(context, sendMessage?.message, Toast.LENGTH_LONG).show()
                                sendMessage?.errors?.forEach {
                                    when (it.path?.get(1)) {
                                        "content" -> sendMoneyDialogView.messageContentEditText.error = it.message
                                        "attachable" -> {
                                            if (it.path.size > 2 && it.path[2] == "amount") {
                                                sendMoneyDialogView.amountEditText.error = it.message
                                            }
                                        }
                                    }
                                }
                            }
                            okButton.isEnabled = true
                            cancelButton.isEnabled = true
                        }
                    }
                    catch(e: ApolloNetworkException) {
                        (activity as TalkActivity).handleApolloException(e)
                        withContext(Dispatchers.Main) {
                            okButton.isEnabled = true
                            cancelButton.isEnabled = true
                        }
                    }
                }
            }
        }

        return sendMoneyDialog
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::assetJob.isInitialized) { assetJob.cancel() }
    }

    /**
     * Load assets data
     */
    @ExperimentalCoroutinesApi
    private fun loadAsset() {
        val assetsQueryChannel = Queries.getAssetsQueryChannel(context as Context)

        assetJob = CoroutineScope(Dispatchers.IO).launch {
            try {
                assetsQueryChannel.consumeEach {
                    val asset = it.data()?.assets?.first()
                    val params = asset?.currency?.allowedTransactions?.find { t -> t.id == "transfer" }

                    if (params != null) {
                        withContext(Dispatchers.Main) {
                            val currencyString = getString(R.string.amount_iso_code_format, asset.currency.exponent, asset.currency.isoCode)

                            sendMoneyDialogView.availableBalanceTextView.text = currencyString.format(asset.balance)

                            sendMoneyDialogView.amountEditText.addTextChangedListener { a ->
                                val amount = try { a.toString().toDouble() }
                                catch(e: NumberFormatException) { 0.0 }

                                when {
                                    amount < params.minAmount as Double -> {
                                        sendMoneyDialogView.totalAmountTextView.text = currencyString.format(0.0)
                                        sendMoneyDialogView.feeTextView.text = currencyString.format(0.0)
                                        sendMoneyDialogView.willReceiveTextView.text = currencyString.format(0.0)
                                    }
                                    ((amount * params.feePercentage as Double) / (1 - params.feePercentage)) <= params.minFee as Double -> {
                                        sendMoneyDialogView.totalAmountTextView.text = currencyString.format(amount + params.minFee)
                                        sendMoneyDialogView.feeTextView.text = currencyString.format(-params.minFee)
                                        sendMoneyDialogView.willReceiveTextView.text = currencyString.format(amount)
                                    }
                                    else -> {
                                        val fee = ((amount * params.feePercentage) / (1 - params.feePercentage))
                                        sendMoneyDialogView.totalAmountTextView.text = currencyString.format(amount + fee)
                                        sendMoneyDialogView.feeTextView.text = currencyString.format(fee)
                                        sendMoneyDialogView.willReceiveTextView.text = currencyString.format(amount)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch(e: ApolloNetworkException) {
                (activity as TalkActivity).handleApolloException(e)
            }
        }
    }

    companion object {

        /**
         * Build an instance with [userId] argument
         */
        fun newInstance(userId: String?) = SendMoneyDialogFragment().apply {
            arguments = Bundle().apply { putString(ChatFragment.TALK_ID, userId) }
        }
    }
}
