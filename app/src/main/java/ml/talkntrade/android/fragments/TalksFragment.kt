package ml.talkntrade.android.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.apollographql.apollo.api.Input
import com.apollographql.apollo.coroutines.toChannel
import kotlinx.android.synthetic.main.fragment_talks.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.consumeEach
import ml.talkntrade.android.R
import ml.talkntrade.android.activities.MainActivity
import ml.talkntrade.android.activities.shared.BaseActivity
import ml.talkntrade.android.adapters.TalksAdapter
import ml.talkntrade.android.queries.TalksQuery
import ml.talkntrade.android.utils.GraphQLClient

class TalksFragment : Fragment() {
    private lateinit var talksJob: Job

    companion object {
        fun newInstance() = TalksFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_talks, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).progressBarHandler.show()
        talksList.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
        }
    }

    @ExperimentalCoroutinesApi
    override fun onStart() {
        super.onStart()
        loadTalks()

        (activity as BaseActivity).setOnNewMessageListener {
            loadTalks()
            true
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::talksJob.isInitialized) { talksJob.cancel() }
    }

    @ExperimentalCoroutinesApi
    private fun loadTalks() {
        if (context == null) { return }

        val talksQuery = TalksQuery(Input.optional(1))

        val talksChannel = GraphQLClient(context as Context).apolloClientWithCache.query(talksQuery).toChannel()

        talksJob = CoroutineScope(Dispatchers.IO).launch {
            try {
                talksChannel.consumeEach {
                    it.data()?.talks?.let { talks ->
                        withContext(Dispatchers.Main) {
                            (activity as MainActivity).progressBarHandler.hide()
                            talksList.adapter = TalksAdapter(talks)
                        }
                    }
                }
            }
            catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    (activity as MainActivity?)?.progressBarHandler?.hide()
                    GraphQLClient.handleError(context, e)
                }
            }
        }
    }
}
