package ml.talkntrade.android.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.PopupMenu
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.work.Data
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkInfo
import androidx.work.WorkManager
import com.apollographql.apollo.exception.ApolloNetworkException
import kotlinx.android.synthetic.main.fragment_chat.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.consumeEach
import ml.talkntrade.android.activities.TalkActivity
import ml.talkntrade.android.adapters.MessagesAdapter
import ml.talkntrade.android.collections.Messages
import ml.talkntrade.android.fragment.MessageFields
import ml.talkntrade.android.queries.TalkQuery
import ml.talkntrade.android.utils.DateHelper
import ml.talkntrade.android.utils.Mutations
import ml.talkntrade.android.utils.Queries
import ml.talkntrade.android.workers.SendMessageWorker
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent
import ml.talkntrade.android.R

/**
 * Chat fragment
 */
class ChatFragment : Fragment() {
    private lateinit var talkId: String
    private lateinit var workManager : WorkManager
    private var mContainer: ViewGroup? = null
    private var scrollAtBottom = false
    private val messages = Messages()
    private lateinit var moreMessagesJob: Job

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { talkId = it.getString(TALK_ID) as String }
        workManager = WorkManager.getInstance(context as Context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mContainer = container
        return inflater.inflate(R.layout.fragment_chat, container, false)
    }

    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val messagesLinealLayoutManager = LinearLayoutManager(context)
        messagesLinealLayoutManager.stackFromEnd = true
        messagesRecyclerView.apply {
            setHasFixedSize(true)
            layoutManager = messagesLinealLayoutManager
            adapter = MessagesAdapter(messages)
        }

        loadSendMessageMutation()
        loadDialogSendMoney()
        loadMoreMessagesWhenScrollReachTop()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::moreMessagesJob.isInitialized) { moreMessagesJob.cancel() }
    }

    /**
     * To be called when a new message is received
     */
    fun onNewMessage(message: MessageFields) {
        CoroutineScope(Dispatchers.IO).launch {
            messages.add(message)

            markSeenMessages()

            if (view != null) {
                withContext(Dispatchers.Main) {
                    messagesRecyclerView.adapter?.notifyDataSetChanged()

                    if (scrollAtBottom) {
                        scrollBottom()
                    }
                }
            }
        }
    }

    /**
     * To be called when the talk is reloaded
     */
    fun onTalkChanged(talk: TalkQuery.Talk?) {
        CoroutineScope(Dispatchers.IO).launch {
            messages.clear()
            messages.addAllFromTalkQueryTalk(talk)

            markSeenMessages()

            if (view != null) {
                val firstUnseenMessageIndex = messages.getFirstUnseenMessageIndex()

                withContext(Dispatchers.Main) {
                    messagesRecyclerView.adapter?.notifyDataSetChanged()

                    if (firstUnseenMessageIndex >= 0) {
                        messagesRecyclerView.smoothScrollToPosition(firstUnseenMessageIndex)
                    } else {
                        scrollBottom()
                    }
                }
            }
        }
    }

    /**
     * Move scroll to the bottom
     */
    private fun scrollBottom() {
        if (messages.count() > 0) {
            messagesRecyclerView.smoothScrollToPosition(messages.count() - 1)
        }
    }

    /**
     * Mark unseen messages
     */
    private fun markSeenMessages() {
        val lastUnseenMessage = messages.getLastUnseenMessage()
        if (context != null && lastUnseenMessage != null) {
            val markSeenMessageDeferred = Mutations.getMarkSeenMessageMutationAsync(context as Context, lastUnseenMessage.id)

            CoroutineScope(Dispatchers.IO).launch {
                try { markSeenMessageDeferred.await().data() }
                catch(e: ApolloNetworkException) {
                    (activity as TalkActivity?)?.handleApolloException(e)
                }
            }
        }
    }

    /**
     * Load [SendMessageWorker]
     */
    private fun loadSendMessageMutation() {
        KeyboardVisibilityEvent.setEventListener(activity) { if (it) scrollBottom() }

        messageContentEditText.addTextChangedListener {
            if(it?.isNotEmpty() == true) {
                attachmentsButton.visibility = View.GONE
                sendMessageButton.visibility = View.VISIBLE
            }
            else {
                attachmentsButton.visibility = View.VISIBLE
                sendMessageButton.visibility = View.GONE
            }
        }

        sendMessageButton.setOnClickListener {
            val content = messageContentEditText.text.toString().trim()

            if (content.isNotEmpty()) {
                messageContentEditText.text.clear()

                val messageData = Data.Builder().putString("talkId", talkId).putString("content", content)
                    .build()

                val messageRequest = OneTimeWorkRequestBuilder<SendMessageWorker>().setInputData(messageData)
                    .build()

                workManager.enqueue(messageRequest)

                messages.add(MessageFields("MessageFields", messageRequest.id.toString(), null, content, null, received=false, seen=true))

                messagesRecyclerView.adapter?.notifyItemInserted(messages.count() - 1)
                scrollBottom()

                workManager.getWorkInfoByIdLiveData(messageRequest.id)
                    .observe(this, Observer { workInfo ->
                        if (workInfo != null) {
                            if (workInfo.state == WorkInfo.State.SUCCEEDED) {
                                val messageId = workInfo.outputData.getString("id").toString()
                                val messageCreatedAt = DateHelper().parse(workInfo.outputData.getString("createdAt").toString())

                                messages.remove(messageRequest.id.toString())
                                messages.add(MessageFields("MessageFields", messageId, messageCreatedAt, content, null, received = false, seen = true))
                                messagesRecyclerView.adapter?.notifyDataSetChanged()
                            }
                            else if (workInfo.state == WorkInfo.State.FAILED) {
                                (activity as TalkActivity).refreshConnectionStatus()
                                Toast.makeText(
                                    context,
                                    R.string.failed_to_send_message,
                                    Toast.LENGTH_LONG
                                ).show()
                                messages.remove(messageRequest.id.toString())
                                messagesRecyclerView.adapter?.notifyDataSetChanged()
                            }
                        }
                    })
            }
        }
    }

    /**
     * Load dialog fragment to send money
     */
    @ExperimentalCoroutinesApi
    private fun loadDialogSendMoney() {
        attachmentsButton.setOnClickListener {
            val dropdownMenu = PopupMenu(context as Context, attachmentsButton)
            dropdownMenu.inflate(R.menu.talk_attachments)
            dropdownMenu.setOnMenuItemClickListener {
                if (it.itemId == R.id.sendMoneyItem) {
                    SendMoneyDialogFragment.newInstance(talkId)
                        .show(activity?.supportFragmentManager as FragmentManager, getString(R.string.send_message))
                }
                true
            }
            dropdownMenu.show()
        }
    }

    /**
     * Load more messages the scroll reach to the top
     */
    @ExperimentalCoroutinesApi
    private fun loadMoreMessagesWhenScrollReachTop() {
        messagesRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (messagesProgressBar.visibility == View.GONE && !messagesRecyclerView.canScrollVertically(-1)) {
                    val firstMessageId = messages.getAt(0)?.id
                    if (firstMessageId != null) {
                        messagesProgressBar.visibility = View.VISIBLE
                        val talkMessagesQueryChannel =
                            Queries.getTalkMessagesQueryChannel(context as Context, talkId, firstMessageId)

                        moreMessagesJob = CoroutineScope(Dispatchers.IO).launch {
                            try {
                                talkMessagesQueryChannel.consumeEach {
                                    val insertedCount =
                                        messages.addAllFromTalkMessagesQueryTalk(it.data()?.talk)

                                    markSeenMessages()

                                    withContext(Dispatchers.Main) {
                                        if (insertedCount > 0) {
                                            messagesRecyclerView.adapter?.notifyItemRangeInserted(0, insertedCount)
                                            messagesRecyclerView.smoothScrollToPosition(insertedCount - 1)
                                        }
                                        messagesProgressBar.visibility = View.GONE
                                    }
                                }
                            } catch (e: ApolloNetworkException) {
                                (activity as TalkActivity).handleApolloException(e)
                                withContext(Dispatchers.Main) {
                                    messagesProgressBar.visibility = View.GONE
                                }
                            }
                        }
                    }
                }

                scrollAtBottom = !messagesRecyclerView.canScrollVertically(1)
            }
        })
    }

    companion object {
        const val TALK_ID = "talkId"

        /**
         * Build an instance with [userId] argument
         */
        fun newInstance(userId: String?) = ChatFragment().apply {
            arguments = Bundle().apply { putString(TALK_ID, userId) }
        }
    }
}
