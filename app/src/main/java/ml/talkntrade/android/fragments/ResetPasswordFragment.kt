package ml.talkntrade.android.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.apollographql.apollo.coroutines.toDeferred
import kotlinx.android.synthetic.main.fragment_reset_password.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ml.talkntrade.android.R
import ml.talkntrade.android.activities.LoginActivity
import ml.talkntrade.android.activities.ResetPasswordActivity
import ml.talkntrade.android.mutations.ResetPasswordMutation
import ml.talkntrade.android.type.OtpInput
import ml.talkntrade.android.type.PasswordInput
import ml.talkntrade.android.utils.GraphQLClient

private const val OTP_ID = "otpId"

class ResetPasswordFragment : Fragment() {
    private lateinit var otpId: String

    companion object {
        fun newInstance(otpId: String) = ResetPasswordFragment().apply {
            arguments = Bundle().apply { putString(OTP_ID, otpId) }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { otpId = it.getString(OTP_ID) as String }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_reset_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        resetPasswordButton.setOnClickListener {
            (activity as ResetPasswordActivity).progressBarHandler.show()

            val otpInput = OtpInput(otpId, confirmationCodeEditText.text.toString())
            val attributes = PasswordInput(passwordEditText.text.toString(), passwordConfirmationEditText.text.toString())
            val resetPasswordMutation = ResetPasswordMutation(otpInput, attributes)
            val resetPasswordDeferred = GraphQLClient(context as Context).apolloClient.mutate(resetPasswordMutation).toDeferred()

            CoroutineScope(Dispatchers.IO).launch {
                try {
                    val response = resetPasswordDeferred.await()
                    val resetPassword = response.data()?.resetPassword

                    withContext(Dispatchers.Main) {
                        (activity as ResetPasswordActivity).progressBarHandler.hide()
                        if (resetPassword?.success == true) {
                            Toast.makeText(context, resetPassword.message, Toast.LENGTH_LONG).show()
                            val intent = Intent(activity, LoginActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                            startActivity(intent)
                            activity?.finish()
                        } else {
                            Toast.makeText(context, resetPassword?.message, Toast.LENGTH_LONG).show()
                            resetPassword?.errors?.forEach {
                                if (it.path?.get(0) == "otp" && it.path[1] == "password") {
                                    confirmationCodeEditText.error = it.message
                                } else if (it.path?.get(0) == "attributes") {
                                    when (it.path[1]) {
                                        "password" -> passwordEditText.error = it.message
                                        "passwordConfirmation" -> passwordConfirmationEditText.error = it.message
                                    }
                                }
                            }
                        }
                    }
                }
                catch (e: Exception) {
                    withContext(Dispatchers.Main) {
                        (activity as ResetPasswordActivity).progressBarHandler.hide()
                        Toast.makeText(context, R.string.failed_to_execute_request, Toast.LENGTH_LONG).show()
                    }
                    e.printStackTrace()
                }
            }
        }
    }
}
