package ml.talkntrade.android.fragments

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import ml.talkntrade.android.R

/**
 * Dialog fragment to confirm stuff
 */
class ConfirmationDialogFragment : DialogFragment() {
    private var message: String? = null
    private lateinit var listener: ConfirmationDialogFragmentListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { message = it.getString(MESSAGE) }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        AlertDialog.Builder(activity).setMessage(message ?: activity?.getString(R.string.confirmation))
            .setPositiveButton(android.R.string.ok) { _, _ ->
                if (::listener.isInitialized) {
                    listener.setPositiveButton()
                }
            }
            .setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.cancel() }
            .create()

    /**
     * Called when the OK button has been pressed
     */
    fun setOnPositiveButtonListener(event: () -> Unit) {
        listener = object : ConfirmationDialogFragmentListener {
            override fun setPositiveButton() { event() } }
    }

    companion object {
        const val MESSAGE = "message"

        fun newInstance(message: String) = ConfirmationDialogFragment().apply {
            arguments = Bundle().apply { putString(MESSAGE, message) }
        }
    }

    interface ConfirmationDialogFragmentListener {
        fun setPositiveButton()
    }
}
