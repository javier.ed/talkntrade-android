package ml.talkntrade.android.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.apollographql.apollo.coroutines.toDeferred
import kotlinx.android.synthetic.main.fragment_request_password_reset.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ml.talkntrade.android.R
import ml.talkntrade.android.activities.ResetPasswordActivity
import ml.talkntrade.android.mutations.RequestPasswordResetMutation
import ml.talkntrade.android.utils.GraphQLClient

class RequestPasswordResetFragment : Fragment() {
    companion object {
        fun newInstance() = RequestPasswordResetFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_request_password_reset, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requestPasswordResetButton.setOnClickListener {
            (activity as ResetPasswordActivity).progressBarHandler.show()

            val requestPasswordResetMutation = RequestPasswordResetMutation(loginEditText.text.toString())
            val requestPasswordResetDeferred =
                GraphQLClient(context as Context).apolloClient.mutate(requestPasswordResetMutation).toDeferred()

            CoroutineScope(Dispatchers.IO).launch {
                try {
                    val response = requestPasswordResetDeferred.await()
                    val requestPasswordReset = response.data()?.requestPasswordReset

                    withContext(Dispatchers.Main) {
                        (activity as ResetPasswordActivity).progressBarHandler.hide()
                        if (requestPasswordReset?.success == true) {
                            Toast.makeText(context, requestPasswordReset.message, Toast.LENGTH_LONG).show()
                            activity?.supportFragmentManager?.beginTransaction()
                                ?.replace(
                                    R.id.resetPasswordContainer,
                                    ResetPasswordFragment.newInstance(requestPasswordReset.otpId.toString())
                                )
                                ?.commitNow()
                        } else {
                            Toast.makeText(context, requestPasswordReset?.message, Toast.LENGTH_LONG).show()
                        }
                    }
                } catch (e: Exception) {
                    withContext(Dispatchers.Main) {
                        (activity as ResetPasswordActivity).progressBarHandler.hide()
                        Toast.makeText(context, R.string.failed_to_execute_request, Toast.LENGTH_LONG).show()
                    }
                    e.printStackTrace()
                }
            }
        }
    }
}
