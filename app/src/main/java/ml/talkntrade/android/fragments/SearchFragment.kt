package ml.talkntrade.android.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.apollographql.apollo.coroutines.toChannel
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.fragment_search.view.usersList
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.consumeEach
import ml.talkntrade.android.R
import ml.talkntrade.android.activities.MainActivity
import ml.talkntrade.android.adapters.UsersAdapter
import ml.talkntrade.android.queries.UsersQuery
import ml.talkntrade.android.utils.GraphQLClient

class SearchFragment : Fragment() {
    private lateinit var usersJob: Job
    private var usersCount: Int = 0

    companion object {
        fun newInstance() = SearchFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.usersList.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        cancelJob()
    }

    private fun cancelJob() {
        if (::usersJob.isInitialized) { usersJob.cancel() }
    }

    @ExperimentalCoroutinesApi
    fun searchUsers(query: String) {
        cancelJob()
        val queryTrimmed = query.trim()
        if (queryTrimmed.length >= 5) {
            if (usersCount == 0) { (activity as MainActivity).progressBarHandler.show() }
            val usersQuery = UsersQuery(queryTrimmed)
            val usersChannel = GraphQLClient(context as Context).apolloClientWithCache.query(usersQuery).toChannel()

            usersJob = CoroutineScope(Dispatchers.IO).launch {
                try {
                    usersChannel.consumeEach {
                        it.data()?.users?.let { users ->
                            usersCount = users.count()
                            withContext(Dispatchers.Main) {
                                (activity as MainActivity).progressBarHandler.hide()
                                usersList.adapter = UsersAdapter(users)
                            }
                        }
                    }
                }
                catch (e: Exception) {
                    withContext(Dispatchers.Main) {
                        (activity as MainActivity?)?.progressBarHandler?.hide()
                        GraphQLClient.handleError(context, e)
                    }
                }
            }
        }
        else {
            (activity as MainActivity).progressBarHandler.hide()
            usersList.adapter = UsersAdapter(listOf())
        }
    }
}
