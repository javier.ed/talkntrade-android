package ml.talkntrade.android.data

import android.content.Context
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.security.Keys
import ml.talkntrade.android.BuildConfig
import ml.talkntrade.android.data.shared.BasePrefs
import java.util.*

/**
 * Current device's data
 */
class CurrentDevice(context: Context) : BasePrefs("current_device", context) {
    var id: String? = null
        get() = prefs.getString("id", null)
        set(value) {
            edit = edit.putString("id", value)
            field = value
        }

    var secretKey: String? = null
        get() = prefs.getString("secretKey", null)
        set(value) {
            edit = edit.putString("secretKey", value)
            field = value
        }

    val token: String? by lazy {
        if (secretKey.isNullOrBlank()) return@lazy null

        val key = Keys.hmacShaKeyFor(secretKey.toString().toByteArray())
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.HOUR, -12)
        val notBefore = calendar.time
        calendar.add(Calendar.HOUR, 24)
        val expiration = calendar.time

        Jwts.builder()
            .setNotBefore(notBefore)
            .setExpiration(expiration)
            .claim("app_id", BuildConfig.APP_ID)
            .claim("session_id", CurrentSession(context).id)
            .signWith(key).compact()
    }

    fun update(id: String? = this.id, secretKey: String? = this.secretKey) : Boolean {
        this.id = id
        this.secretKey = secretKey
        return save()
    }
}

