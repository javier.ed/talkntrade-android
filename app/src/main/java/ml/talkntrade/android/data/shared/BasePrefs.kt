package ml.talkntrade.android.data.shared

import android.content.Context
import android.content.SharedPreferences

/**
 * To handle data stored in [SharedPreferences]
 *
 * @param [storeIn] Shared preferences name
 */
abstract class BasePrefs(storeIn: String, context: Context) {
    protected val prefs: SharedPreferences = context.getSharedPreferences(storeIn, Context.MODE_PRIVATE)
    protected var edit: SharedPreferences.Editor = prefs.edit()

    fun save() = edit.commit()

    fun delete() = prefs.edit().clear().commit()
}
