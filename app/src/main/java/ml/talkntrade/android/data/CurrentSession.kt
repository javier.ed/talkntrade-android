package ml.talkntrade.android.data

import android.content.Context
import ml.talkntrade.android.data.shared.BasePrefs

/**
 * Current session's data
 */
class CurrentSession(context: Context) : BasePrefs("current_session", context) {
    var id: String? = null
        get() = prefs.getString("id", null)
        set(value) {
            edit = edit.putString("id", value)
            field = value
        }

    fun update(id: String? = this.id) : Boolean {
        this.id = id
        return save()
    }
}

