package ml.talkntrade.android.data

import android.content.Context
import ml.talkntrade.android.data.shared.BasePrefs

/**
 * Current's user data
 */
class CurrentUser(context: Context) : BasePrefs("current_user", context) {
    var id: String? = null
        get() = prefs.getString("id", null)
        set(value) {
            edit = edit.putString("id", value)
            field = value
        }

    var username: String? = null
        get() = prefs.getString("username", null)
        set(value) {
            edit = edit.putString("username", value)
            field = value
        }

    fun update(id: String? = this.id, username: String? = this.username) : Boolean {
        this.id = id
        this.username = username
        return save()
    }
}
