package ml.talkntrade.android.collections

import kotlinx.coroutines.ExperimentalCoroutinesApi
import ml.talkntrade.android.R
import ml.talkntrade.android.fragment.MessageFields
import ml.talkntrade.android.fragments.ChatFragment
import ml.talkntrade.android.queries.TalkQuery

/**
 * Manages the list of fragment at Talk activity view pager
 */
class TalkFragments(talkId: String?) {
    private val fragments by lazy {
        mutableListOf(
            Pair(R.string.chat, ChatFragment.newInstance(talkId))
        )
    }

    /**
     * To get the title of a fragment by the [position]
     */
    fun getTitle(position: Int) = fragments[position].first

    /**
     * To get a fragment by the [position]
     */
    fun getFragment(position: Int) = fragments[position].second

    /**
     * To get [ChatFragment]
     */
    private fun getChatFragment() = getFragment(0) as ChatFragment

    /**
     * Total amount of fragments
     */
    fun count() = fragments.size

    /**
     * To be called a new message is received from ConnectionService
     */
    @ExperimentalCoroutinesApi
    fun onNewMessage(message: MessageFields) {
        getChatFragment().onNewMessage(message)
    }

    /**
     * To be called the talk is reloaded
     */
    fun onTalkChanged(talk: TalkQuery.Talk) {
        getChatFragment().onTalkChanged(talk)
    }
}
