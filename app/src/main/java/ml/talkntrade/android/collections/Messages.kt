package ml.talkntrade.android.collections

import ml.talkntrade.android.fragment.MessageFields
import ml.talkntrade.android.queries.TalkMessagesQuery
import ml.talkntrade.android.queries.TalkQuery

/**
 * Manages a list of messages
 */
class Messages {
    private val messages = mutableListOf<MessageFields>()

    /**
     * To get a message by [id]
     *
     * @return Message found or `null`
     */
    fun get(id: String) = messages.find { m -> m.id == id }

    /**
     * To get a message by [index]
     *
     * @return Message found or `null`
     */
    fun getAt(index: Int) = messages.getOrNull(index)

    /**
     * To get the position of the first unseen message
     *
     * @return Position or -1 if none has been found
     */
    fun getFirstUnseenMessageIndex() = messages.indexOfFirst { m -> m.received == true && m.seen != true }

    /**
     * To get the last unseen message
     *
     * @return Message found or `null`
     */
    fun getLastUnseenMessage() = messages.findLast {  m -> m.received == true && m.seen != true }

    /**
     * Adds a message to the list
     *
     * @return `true` if the message is added, or `false` is the message already exists
     */
    fun add(message: MessageFields) : Boolean {
        return when {
            message.createdAt == null -> messages.add(message)
            get(message.id) == null -> {
                val index = messages.indexOfFirst { m -> m.createdAt != null && m.createdAt > message.createdAt }
                if (index >= 0) {
                    messages.add(index, message)
                } else {
                    val unsentIndex = messages.indexOfFirst { m -> m.createdAt == null }
                    if (unsentIndex >= 0) {
                        messages.add(unsentIndex, message)
                    } else {
                        messages.add(message)
                    }
                }
                true
            }
            else -> {
                val index = messages.indexOfFirst { m -> m.id == message.id }
                messages[index] = message
                false
            }
        }
    }

    /**
     * Adds all messages from `TalkQuery.Talk`
     *
     * @return Amount of messages added
     */
    fun addAllFromTalkQueryTalk(talk: TalkQuery.Talk?) : Int {
        var insertedCount = 0

        talk?.messages?.forEach { m ->
            if (add(m.fragments.messageFields)) {
                insertedCount++
            }
        }
        return insertedCount
    }

    /**
     * Adds all messages from `TalkMessagesQuery.Talk`
     *
     * @return Amount of messages added
     */
    fun addAllFromTalkMessagesQueryTalk(talk: TalkMessagesQuery.Talk?) : Int {
        var insertedCount = 0

        talk?.messages?.forEach { m ->
            if (add(m.fragments.messageFields)) {
                insertedCount++
            }
        }
        return insertedCount
    }

    /**
     * Clear messages list
     */
    fun clear() = messages.clear()

    /**
     * Removes the message with the matching [id]
     */
    fun remove(id: String) = messages.removeAll { m -> m.id == id }

    /**
     * Total amount of messages in the list
     */
    fun count() = messages.size
}
