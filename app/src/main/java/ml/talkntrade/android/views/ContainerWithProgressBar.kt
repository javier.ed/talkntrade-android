package ml.talkntrade.android.views

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import kotlinx.android.synthetic.main.container_with_progress_bar.view.*
import ml.talkntrade.android.R

/**
 * Custom FrameLayout with a progress bar
 */
class ContainerWithProgressBar @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) :
    FrameLayout(context, attrs, defStyle) {
    init {
        LayoutInflater.from(context).inflate(R.layout.container_with_progress_bar, this, true)

        val styledAttrs = context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.ContainerWithProgressBar,
            0, 0
        )

        containerContent.visibility = getStyledAttrValue(styledAttrs, R.styleable.ContainerWithProgressBar_container_visibility, View.VISIBLE)
        containerProgressBar.visibility = getStyledAttrValue(styledAttrs, R.styleable.ContainerWithProgressBar_progress_bar_visibility, View.GONE)
        styledAttrs.recycle()
    }

    private fun getStyledAttrValue(styledAttrs: TypedArray, index: Int, defValue: Int) = styledAttrs.getInteger(index, defValue)

    var contentVisibility: Int = View.VISIBLE
    get() = containerContent.visibility
    set(value) {
        containerContent.visibility = value
        field = value
    }

    var progressBarVisibility: Int = View.GONE
        get() = containerProgressBar.visibility
        set(value) {
            containerProgressBar.visibility = value
            field = value
        }

    override fun addView(child: View, index: Int, params: ViewGroup.LayoutParams) {
        if (containerContent == null) {
            super.addView(child, index, params)
        }
        else {
            containerContent.addView(child, index, params)
        }
    }

    /**
     * To show content
     *
     * @param [hideProgressBar] Hide the progress bar by default
     */
    fun showContent(hideProgressBar: Boolean = true) {
        contentVisibility = View.VISIBLE
        if (hideProgressBar) { progressBarVisibility = View.GONE }
    }

    /**
     * To show progress bar
     *
     * @param [hideContent] Hide the content bar by default
     */
    fun showProgressBar(hideContent: Boolean = true) {
        if (hideContent) { contentVisibility = View.GONE }
        progressBarVisibility = View.VISIBLE
    }
}
