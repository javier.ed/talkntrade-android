package ml.talkntrade.android.workers

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import ml.talkntrade.android.utils.DateHelper
import ml.talkntrade.android.utils.Mutations

/**
 * Background worker to send a message
 */
class SendMessageWorker(context: Context, params: WorkerParameters) : CoroutineWorker(context, params) {
    override suspend fun doWork(): Result {
        val content = inputData.getString("content").toString()
        val talkId = inputData.getString("talkId").toString()

        val sendMessageMutationAsync = Mutations.getSendMessageMutationAsync(applicationContext, talkId, content)

        return try {
            val response = sendMessageMutationAsync.await().data()
            val sendMessage = response?.sendMessage

            if (sendMessage?.success == true) {
                val messageData = workDataOf(
                    "id" to sendMessage.sentMessage?.id,
                    "createdAt" to DateHelper.stringify(sendMessage.sentMessage?.createdAt)
                )

                Result.success(messageData)
            }
            else { Result.failure() }
        }
        catch(e: Exception) {
            Result.failure()
        }
    }
}
