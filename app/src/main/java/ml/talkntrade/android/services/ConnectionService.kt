package ml.talkntrade.android.services

import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Binder
import android.os.IBinder
import com.apollographql.apollo.exception.ApolloNetworkException
import com.google.gson.Gson
import com.google.gson.internal.LinkedTreeMap
import kotlinx.coroutines.*
import ml.talkntrade.android.activities.TalkActivity
import ml.talkntrade.android.models.NewMessage
import ml.talkntrade.android.models.Subscription
import ml.talkntrade.android.subscriptions.NewMessageSubscription
import ml.talkntrade.android.utils.SubscriptionsHandler
import ml.talkntrade.android.utils.NotificationsHandler
import ml.talkntrade.android.utils.Queries

/**
 * Service that handle the connection with the server
 */
class ConnectionService : Service() {
    private val binder = ConnectionServiceBinder()
    private val listeners = mutableListOf<Pair<String, ConnectionServiceListener>>()
    private var notificationsHandler : NotificationsHandler? = null
    private var subscriptionsHandler : SubscriptionsHandler? = null
    var currentStatusCode = STATUS_DISCONNECTED

    inner class ConnectionServiceBinder : Binder() {
        fun getService() = this@ConnectionService
    }

    @ExperimentalCoroutinesApi
    override fun onCreate() {
        super.onCreate()

        notificationsHandler = NotificationsHandler(this)

        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val builder = NetworkRequest.Builder()
        builder.addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
        builder.addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
        builder.addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
        builder.addTransportType(NetworkCapabilities.TRANSPORT_ETHERNET)
        builder.addTransportType(NetworkCapabilities.TRANSPORT_VPN)

        val callback = object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network?) {
                super.onAvailable(network)
                changeConnectionStatus(STATUS_CONNECTING)
                loadInfo()
            }

            override fun onLost(network: Network) {
                super.onLost(network)
                stopSubscriptions()
                changeConnectionStatus(STATUS_DISCONNECTED)
            }

            override fun onUnavailable() {
                super.onUnavailable()
                stopSubscriptions()
                changeConnectionStatus(STATUS_DISCONNECTED)
            }
        }

        connectivityManager.registerNetworkCallback(builder.build(), callback)
    }

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    private fun changeConnectionStatus(newStatus: Int) {
        currentStatusCode = newStatus
        listeners.forEach { it.second.onStatusChange(newStatus) }
    }

    @ExperimentalCoroutinesApi
    private fun loadInfo() {
        val infoQueryDeferred = Queries.getInfoQueryAsync(this)

        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = infoQueryDeferred.await().data()
                val info = response?.info

                val currentStatus = when (info?.clientStatus) {
                    "user_authenticated" -> {
                        startSubscriptions()
                        STATUS_CONNECTING
                    }
                    "device_authorized" -> {
                        stopSubscriptions()
                        STATUS_DEVICE_AUTHORIZED
                    }
                    "unauthorized" -> {
                        stopSubscriptions()
                        STATUS_DEVICE_UNAUTHORIZED
                    }
                    "connection_error" -> {
                        stopSubscriptions()
                        STATUS_CONNECTION_ERROR
                    }
                    else -> {
                        stopSubscriptions()
                        STATUS_UNKNOWN_ERROR
                    }
                }

                changeConnectionStatus(currentStatus)
            }
            catch(e: ApolloNetworkException) {
                e.printStackTrace()

                changeConnectionStatus(STATUS_CONNECTION_ERROR)

                Thread {
                    Thread.sleep(5000)
                    loadInfo()
                }.start()
            }
        }
    }

    /**
     * Start GraphQL subscriptions
     */
    private fun startSubscriptions() {
        subscriptionsHandler = SubscriptionsHandler(this)

        subscriptionsHandler?.apply {
            onConnected = {
                changeConnectionStatus(STATUS_CONNECTED)
                subscribe(NewMessageSubscription())
            }

            onReceived = { data: Any? ->
                if (data is LinkedTreeMap<*, *>) {
                    val subscriptionModel = Gson().fromJson(Gson().toJson(data), Subscription::class.java)
                    val newMessage = subscriptionModel.newMessage

                    if (newMessage != null) {
                        var notify = true
                        listeners.forEach {
                            if (!it.second.onNewMessage(newMessage)) {
                                notify = false
                            }
                        }

                        if (newMessage.received && notify) {
                            val intent = Intent(this@ConnectionService, TalkActivity::class.java).apply {
                                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            }
                            intent.putExtra("ID", newMessage.talk.id)
                            val pendingIntent: PendingIntent = PendingIntent.getActivity(this@ConnectionService, 0, intent, 0)
                            notificationsHandler?.notify(newMessage.talk.name, newMessage.content, pendingIntent)
                        }
                    }
                }
            }

            onDisconnected = { changeConnectionStatus(STATUS_DISCONNECTED) }

            onRejected = { changeConnectionStatus(STATUS_CONNECTION_ERROR) }

            onFailed = { changeConnectionStatus(STATUS_CONNECTION_ERROR) }

            connect()
        }
    }

    /**
     * Stop GraphQL subscriptions
     */
    private fun stopSubscriptions() {
        subscriptionsHandler?.disconnect()
        subscriptionsHandler = null
    }

    @ExperimentalCoroutinesApi
    fun refreshConnectionStatus() {
        loadInfo()
    }

    fun addListener(id: String, listener: ConnectionServiceListener) : Boolean = listeners.add(Pair(id, listener))

    fun removeListener(id: String) : Boolean = listeners.removeAll { it.first == id }

    interface ConnectionServiceListener {
        fun onStatusChange(statusCode: Int) : Boolean
        fun onNewMessage(message: NewMessage) : Boolean
    }

    companion object {
        const val STATUS_DISCONNECTED = 0
        const val STATUS_CONNECTING = 1
        const val STATUS_CONNECTED = 2
        const val STATUS_DEVICE_AUTHORIZED = 3
        const val STATUS_DEVICE_UNAUTHORIZED = 4
        const val STATUS_CONNECTION_ERROR = 5
        const val STATUS_UNKNOWN_ERROR = 6
    }
}
