# Talk n Trade for Android

[![pipeline status](https://gitlab.com/talkntrade/talkntrade-android/badges/master/pipeline.svg)](https://gitlab.com/talkntrade/talkntrade-android/commits/master)

## Requirements

* Git 2.20+
* Java 8
* Kotlin 1.3
* Gradle 5.1.1
* Node.js 8+

## Basic configuration

```
cp talkntrade.example.gradle talkntrade.gradle
```
