<!-- Before opening a new issue, make sure to search for keywords in the issues filtered by the "refactoring" label and verify the issue you're about to submit isn't a duplicate. -->

## Summary

<!-- Please briefly describe what part of the code base needs to be refactored -->

## Improvements

<!-- Explain the benefits of refactoring this code -->

## Risks

<!-- Please list features that can break because of this refactoring and how you intend to solve that -->

## Involved components

<!-- List files or directories that will be changed by the refactoring -->

## Intended side effects

<!--
If the refactoring involves changes apart from the main improvements, list them here.
It may be a good idea to create separate issues and link them here.
-->

## Missing test coverage

<!-- If you are aware of tests that need to be written or adjusted apart from unit tests for the changed components, please list them here. -->

## Links and/or references

<!--
* Any related issues and/or MRs with a short description.
* External links with any relevant information.
-->

/label ~refactoring
