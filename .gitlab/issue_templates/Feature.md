<!-- Before opening a new issue, make sure to search for keywords in the issues filtered by the "feature" label and verify the issue you're about to submit isn't a duplicate. -->

## Problem to solve

<!-- What problem do we solve? -->

## Proposal

<!-- How are we going to solve the problem? -->

## Testing

<!-- What risks does this change pose? How might it affect the quality of the product? What additional test coverage or changes to tests will be needed? -->

## Links and/or references

<!--
* Any related issues and/or MRs with a short description.
* External links with any relevant information.
-->

/label ~feature
