<!-- Before opening a new issue, make sure to search for keywords in the issues filtered by the "documentation" label and verify the issue you're about to submit isn't a duplicate. -->

## Problem to solve

<!--
* What features are affected?
* What docs or doc section affected? Include links or paths.
* Is there a problem with a specific document, or a feature/process that's not addressed sufficiently in docs?
* Any other ideas or requests?
-->

## Proposal

<!-- Further specifics for how can we solve the problem. -->

## Links and/or references

<!--
* Any related issues and/or MRs with a short description.
* External links with any relevant information.
-->

/label ~documentation
