# CHANGELOG

## [0.4.0] - 2019-10-31

### Added

* Feature to report users !96
* Feature to rate products !95
* Features to block and unblock users !94
* Merge request templates !93
* Issue templates !92


## [0.3.0] - 2019-09-30

### Added

* Service to check connection status !91
* Links of transactions in a blockchain explorer !90
* Attribute to make links clickable in messages !81
* AboutActivity !80
* ProgressBar in OrderActivity !78
* ProgressBar in SettingsActivity !77
* Now shows when the message is currently sending !74
* ProgressBar in ProductActivity !70
* ProgressBar in UserActivity !69
* ProgressBar in WalletActivity !68
* Get current fees from the server !67
* Show password in Login activity !66

### Changed

* Center address in deposit dialog !88
* Start searching users when the query has at least 5 characters !84
* minSdkVersion increased to 21 !83
* Some improvements in TalkActivity !65
* Finish LoginActivity when the device cannot be registered !61
* Upgrade dependencies !60
* The app name is now Talk'nTrade !56
* New logo !50

### Fixed

* Restored .graphqlconfig for js-graphql plugin of IntelliJ IDEA !89 !87
* Disable buttons on submit !86
* Assign current talk Id on TalkActivity restart !85
* Some linter warnings !82 !62
* New message receiver with attachable !75
* NewMessageService cannot be exported !73
* LICENSE !71
* Better use of activities life cycles to avoid loading content twice !64
* Avoid sending multiple simultaneous requests in MainActivity !63
* Typo in LoginActivity !58
* Remove files in NewProductActivity !54
* Prevent obfuscation of ProdutInput & ProductFileInput in proguard-rules.pro !52

### Removed

* UnsafeOkHttpCLient !79
* Remove unnecessary logging !76


## [0.2.0] - 2019-09-02

### Added

* Show user details !49
* Users can now pay their shopping carts !48
* Users can now remove items from their shopping cart !47
* Users can now add items to their shopping cart !46
* Users can now see the products of others !44
* Users can now finish their sales manually !43
* Users can now start sales created previously !42
* Users can now see the sales of their products !41
* Users can now create sales from their products !40
* Users can now edit their products !39
* Displays current user products !38
* User can now create products !37
* Update profile feature !35
* Search users feature !32

### Changed

* Upgrade dependencies !45 !33
* Force to use screen orientation in portrait mode !36

### Fixed

* Using cache in GitLab CI !34


## [0.1.0] - 2019-07-02

### Added

* Password reset feature !29
* Email settings !28
* Change password !26
* Main activity drawer icons added !25
* Set response locale in request headers !19
* Mark seen messages !17
* Talk item with last message & unseen messages quantity !16

### Changed

* New logo !31
* Change hosts to core.talkntrade.ml & testnet.talkntrade.ml !30
* Upgrade Kotlin to 1.3.40 !27
* Upgrade dependencies !20
* Handle cached queries with coroutines !18

### Fixed

* Adding some strings with spanish translations !24
* Securing new message service !23
* Avoid sending money multiple times when the Ok button is clicked more than once !22
* Fix server typo !21
* Fixed crash in Android 4.x by downgrading OkHttpClient to 3.12 & adding multidex support !15
* Reload talks on resume MainActivity !15
* Added margins programmatically to messages !15


## [0.0.0] - 2019-05-28

* Add LICENSE !14
* Add .gitlab-ci.yml !13
* Add README !12
* Add .editorconfig !11
* UI improvements !10
* Build flavors for Testnet (demo) & Mainnet !9
* Connection improvements !8
* BTC transfers in messages !7
* BTC deposits & withdrawals !6
* Upgrade Apollo !5
* Wallets feature !4
* Infinite scroll in talk messages !3
* Talks feature !2
* Register, login and logout !1
* All is new
